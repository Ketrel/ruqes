#!/usr/bin/env bash

git log $(git describe --tags --abbrev=0)..HEAD --color --format=%B | grep -Ev '^$' | sed -r 's/^/* /' | tac
