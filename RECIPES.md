# Post rule recipes

## Semi-hide posts from a guild
```js
// ==RuqES_PostRule==
// @name SemiHideGuild
// ==/RuqES_PostRule==

if (post.guild === 'ClownWorld') return A.semiHideAction();
```

## Semi-hide posts from more guilds
```js
// ==RuqES_PostRule==
// @name SemiHideMoreGuilds
// ==/RuqES_PostRule==

const guilds = [
    'OffensiveJokes',
    'LGBT',
];
if (guilds.includes(post.guild)) return A.semiHideAction();
```

## Blur thumbnail of NSFW posts
```js
// ==RuqES_PostRule==
// @name BlurNsfwThumbnails
// ==/RuqES_PostRule==
if (post.nsfw) return A.blurThumbnailAction();
```

## Hide voted-on posts
```js
// ==RuqES_PostRule==
// @name HideVotedOn
// ==/RuqES_PostRule==
if (post.vote) return A.hideAction();
```

## Show only NSFW posts

Before you ask why would anybody need/want this, this was a request from a user (I believe on Discord).

```js
// ==RuqES_PostRule==
// @name OnlyNsfw
// ==/RuqES_PostRule==
if (!post.nsfw) return A.hideAction();
```

## Semi-hide posts with score bellow zero
```js
// ==RuqES_PostRule==
// @name SemiHideNegativeScore
// ==/RuqES_PostRule==
if (post.score < 0) return A.semiHideAction();
```

## Hide posts with a link to domain
```js
// ==RuqES_PostRule==
// @name HideLinkingToDomain
// ==/RuqES_PostRule==
if (post.link && post.link.startsWith('https://files.catbox.moe')) return A.hideAction();
```

## Hide posts with a link to specific domains
```js
// ==RuqES_PostRule==
// @name HideLinkingToDomains
// ==/RuqES_PostRule==
const blocked = [
  'https://i.ruqqus.com',
  'https://youtu.be',
  'https://www.youtube.com',
];
if (blocked.some(function (x){ return (post.link || '').startsWith(x)}))
  return A.hideAction();
```

## Semi-hide posts older than a day
```js
// ==RuqES_PostRule==
// @name SemiHideOlderThanDay
// ==/RuqES_PostRule==
const daySpan = 24 * 60 * 60 * 1000;
if ((new Date(post.date)).getTime() + daySpan < Date.now())
    return A.semiHideAction();
```

## Highlight pinned posts
```js
// ==RuqES_PostRule==
// @name HighlightPinned
// ==/RuqES_PostRule==
if (post.pinned) return A.setBackgroundAction('rgba(255, 0, 255, 0.1)');
```
