# RuqES

RuqES is a userscript, small piece of code which is installed in your browser and can alter how sites look and work, in this case [Ruqqus.com](https://ruqqus.com).

For a list of features and more details (e.g. installation), please see https://ruqqus.com/post/ldx/what-is-ruqes.

# Post rules

Only ES5 syntax is supported, so sadly no fat arrows `=>` for us (but functions like `startsWith` or `includes` are working).

For a quick start see [RECIPES](RECIPES.md). API is available [here](src/modules/post/postRulesApiTypes.ts).

# Development

## Setup

Install appropriate [Node](https://nodejs.org) version (stated in [.node-version](.node-version)). You can [n](https://github.com/tj/n) or other Node managers.

```sh
$ npm i
```

## Watch mode

```sh
$ npm start
```

Install proxy script from: `http://localhost:8080/ruqes.proxy.user.js`.

## Build

```sh
$ npm run build
```

Output is located in `dist` directory.

# TODO
- link handler for guilds
- add creation post rules from recipes
- option for expando button to span post title and actions
- refactor processors to classes
- refactor settings so module containers are located in corresponding module classes
- remove deprecated features (probably in ver. 20)

# Ideas
- thumbnail icon for links to ruqqus?
- add link to 'Saved posts' to feeds section in left sidebar
- comment save (probably just a light version)
- spoiler tag (reddit: `>!spoiler!<`, commonmark plugin: `>>!spoiler!<<`, discord: `||spoiler||`)
- syntax highlighting in code tag
- temporarily enable specific post rule for given tab
- post rules engine: option to disable short circuit?
- support for older GM_ API?
- keyboard shortcuts for next post and expando - https://ruqqus.com/post/1e8v/suggestion-keyboard-shortcuts
  - maybe for start just a select with few options? or jump straight in any key/mouse button way?
- hover to view images (probably just post thumbnails)
- stats in user profile - e.g. total number, score, avg. score of posts and comments (probably when you reach end of infinite scroll?)
- some repost check assistance (search by title, maybe by domain + title/url on ddg)
- destroy embeds (e.g. a YT/BC video) when closing expando button?
- customizable font size, font family
- stats of user's rep (chart)
- card mode (auto-open expando button, don't load post content automatically, add new button to load post content)
- "my guilds" in sidebar could become an icon grid (user specifies guild names and the script replaces content with guild icons without names; customizable tags/icons?)
- change default sorting/limit of specific guild
- counters with up/down-votes of other users
- join/leave button from post on a list - probably not possible without tracking already joined guilds, but it could be done in expando box
- option to hide avatars, images and emojis (replace with asciimoji?)

# License
AGPL3
