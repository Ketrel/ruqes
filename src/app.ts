import {setupStyles} from 'common/styles';
import {isDarkTheme, logPrefix, scriptInfo, scriptVersion} from 'common/common';
import {setupSettings} from 'common/settings';
import {handleFirstSetupOfModules, registerClassModule} from 'common/modules';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';
import {VotingModule} from 'modules/VotingModule';
import {SidebarModule} from 'modules/SidebarModule';
import {CommentModule} from 'modules/CommentModule';
import {CreatePostModule} from 'modules/createPost/CreatePostModule';
import {InfiniteScrollModule} from 'modules/InfiniteScrollModule';
import {PostsModule} from 'modules/post/PostsModule';
import {readConfig} from 'common/config';
import {setupDebugEventListeners} from 'common/events';
import {AdvancedModule} from 'modules/AdvancedModule';
import {SaveModule} from 'modules/save/SaveModule';
import {setupGlobalApi} from './common/RuqESApi';

const printInitMessage = () => {
  const bg = 'background-color: lightyellow;';
  const a = 'color: green; font-weight: bold; padding: 1em 0 1em 1em;' + bg;
  const b = 'color: black; padding: 1em 0; ' + bg;
  const c = 'color: purple; font-weight: bold; padding: 1em 1em 1em 0; ' + bg;
  const d = '';
  const info = GM.info;
  console.log(`%c${logPrefix} ${scriptVersion}%c created by %cenefi%c\n\n'${scriptInfo.name}' running via ${info.scriptHandler} ${info.version} on ${JSON.stringify((info as any).platform)}`, a, b, c, d);
};

const init = () => {
  printInitMessage();
  [
    new InfiniteScrollModule(),
    new ExpandoButtonModule(),
    new VotingModule(),
    new PostsModule(),
    new CommentModule(),
    new SidebarModule(),
    new CreatePostModule(),
    new AdvancedModule(),
    new SaveModule(),
  ].forEach(registerClassModule);
};

const setupDebug = async () => {
  const cfg = await readConfig();
  if (cfg.debug.enabled) {
    setupDebugEventListeners();
  }
};

const work = async () => {
  setupStyles(isDarkTheme());
  await setupSettings();
  await handleFirstSetupOfModules();
  await setupDebug();
  await setupGlobalApi();
};

init();
$(work);

