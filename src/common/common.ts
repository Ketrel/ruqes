import {range, dropWhile, is, tryCatch} from 'ramda';
import moment from 'moment';

import {
  loadingDotsCls,
  ruqesMarkCls,
  textLoaderCls,
  UpDownVotesAsTextDownCls,
  upDownVotesAsTextUpCls,
  upDownVotesAsTextWrapperCls
} from 'common/styles';
import logoSvg from 'assets/logo.svg';
import parseMessyTime from "parse-messy-time";

export const logPrefix = '[RuqES]';
export const scriptInfo = GM.info.script
export const scriptVersion = scriptInfo.version;

let debugEnabled = false;
export const isDebugEnabled = () => debugEnabled;
export const enabledDebug = () => debugEnabled = true;

export const debugLog = (...xs: any[]) => isDebugEnabled() && console.log(logPrefix, '[D]', ...xs);
export const printLog = (...xs: any[]) => console.log(logPrefix, ...xs);
export const printError = (...xs: any[]) => console.error(logPrefix, ...xs);
export const $c = (cls: string, parent?: JQuery) => $(`.${cls}`, parent);
export const $i = (cls: string, parent?: JQuery) => $(`#${cls}`, parent);
export const setClass = (el: JQuery, cls: string, value: boolean) => {
  if (value) {el.addClass(cls);} else {el.removeClass(cls);}
  return el;
};

export const createLoadingDots = () => {
  const container = $('<span>').addClass(loadingDotsCls);
  range(0, 3).map(() => $('<i>.</i>')).forEach(x => container.append(x));
  return container;
};

export const createRuqesMark = () => $('<span>').html(logoSvg).addClass(ruqesMarkCls);

export const createTextLoader = (text: string, cls: string) =>
  $('<div>')
    .addClass(textLoaderCls)
    .addClass(cls)
    .append(createRuqesMark())
    .append($('<span>').text(text).css('margin-left', '0.33em'))
    .append(createLoadingDots())

export const isDarkTheme = () => $('#dark-switch').hasClass('fa-toggle-on');

export const xhr = (cfg: GM.Request) => GM.xmlHttpRequest(cfg);

export const preventDefaults = (evt: Event | JQuery.Event) => {
  evt.preventDefault();
  evt.stopPropagation();
};

export const dropWhileString = (pred: (x: string) => boolean, y: string): string => (dropWhile as any)(pred, y);

interface RuqqusWindow {
  upvote(event: MouseEvent): void;

  downvote(event: MouseEvent): void;
}

export const getRuqqusWindow = () => unsafeWindow as any as RuqqusWindow;

export const getWindowEl = (): JQuery => $(window as any);

export const setupVoting = (el: JQuery): void => {
  el.find('.upvote-button').each((_, rawEl) => {
    rawEl.addEventListener('click', getRuqqusWindow().upvote, false);
  });
  el.find('.downvote-button').each((_, rawEl) => {
    rawEl.addEventListener('click', getRuqqusWindow().downvote, false);
  });
}

export const isString = (x: unknown): x is string => typeof x === 'string';
export const isObject = (x: unknown): x is object => is(Object, x);
export const isArray = (x: unknown): x is unknown[] => is(Array, x);

export const isValidCssColor = (x: string): boolean =>
  /^\w+$|^#[a-f0-9]{3,6}$|^rgba?\(\d+,\s*\d+,\s*\d+(:?,\s*[0-9\.]+)?\)$/.test(x)

export const lastIndex = (xs: unknown[]): number => xs.length - 1;

export const showConfirmDialog = (message: string): Promise<boolean> => Promise.resolve(window.confirm(message));

export const genTextUpDownCounters = (upCount: number, downCount: number, upClasses?: string, downClasses?: string): JQuery => {
  const upEl = $('<span>').text('+' + upCount).addClass(upDownVotesAsTextUpCls).addClass(upClasses);
  const downEl = $('<span>').text('-' + downCount).addClass(UpDownVotesAsTextDownCls).addClass(downClasses);
  return $('<div>').addClass('d-inline-block ml-1').addClass(upDownVotesAsTextWrapperCls).append(' · ').append(upEl).append(' | ').append(downEl);
};

export const parseUpDownCounts = (x: string): [number, number] =>
  (x || '0 | 0').split(/ \| /).map(x => Number.parseInt(x, 10)).map(Math.abs) as [number, number]; // improve types?

export interface PostInfo {
  title: string;
  author: string | null;
  guild: string;
  id: string;
  nsfw: boolean;
  score: number;
  vote: 'up' | null | 'down',
  url: string;
  link: string | null;
  thumbnail: string | null;
  date: string;
  dateRelative: string;
  dateRaw: string;
  pinned: boolean;
  previewModal: boolean;
}

const defaultTextThumbnail = '/assets/images/icons/default_thumb_text.png';
const defaultLinkThumbnail = '/assets/images/icons/default_thumb_link.png';

export const extractPostInfo = (el: JQuery): PostInfo => {
  const dateEl = el.find('.card-block > .post-meta > span[data-toggle="tooltip"]').first();
  const upvoted = el.find('.arrow-up.active').length > 0;
  const downvoted = el.find('.arrow-down.active').length > 0;
  const postId: string = el.prop('id').replace(/^post-/, '');
  const dateRaw = dateEl.prop('title');
  const date = moment(dateRaw, '%DD %MMMM %YYYY at %HH:%mm:%ss').format();
  let guild = el.find('.post-meta-guild a').first().text().trim().replace(/^\+/, '');
  if (!guild) { guild = $i('guild-name-reference').text(); }
  const link = el.find('.card-header a').prop('href') ?? null;
  let thumbnail = el.find('.post-img').prop('src');
  if (!thumbnail) { thumbnail = link ? defaultLinkThumbnail : defaultTextThumbnail; }
  return {
    title: el.find('.post-title').text().trim(),
    author: el.find('.user-name').first().text(),
    guild,
    id: postId,
    nsfw: el.find('.text-danger:contains(nsfw)').length > 0,
    score: Number.parseInt(el.find('.score').last().text()) ?? 0,
    vote: upvoted ? 'up' : downvoted ? 'down' : null,
    url: el.find('.post-actions .fa-comment-dots').parent().prop('href'),
    link,
    date,
    dateRaw,
    dateRelative: dateEl.text(),
    pinned: el.find('i.fa-thumbtack').length > 0,
    thumbnail,
    previewModal: el.find('.card-header a[data-toggle="modal"]').length > 0
  }
};

export const removeLeadingSlash = (x: string) => x.replace(/^\//, '');

export const isOnExactPathGen = (path: string) => () => {
  const currentPath = window.location.pathname;
  return removeLeadingSlash(path) === removeLeadingSlash(currentPath);
}

export const isOnPathOrSubpageGen = (firstPathNamePart: string) => () => {
  const currentPath = window.location.pathname;
  const paths = currentPath.split('/').filter(x => x !== '');
  return removeLeadingSlash(firstPathNamePart) === paths[0];
}

export const isOnPostDetail = isOnPathOrSubpageGen('post');

const hashPrefix = 'RuqES:';

export const isOnRuqESUrl = (path: string): boolean => {
  if (!isOnExactPathGen('')) { return false; }
  const hash = window.location.hash;
  return hash === `#${hashPrefix}${path}`;
};

export const genRuqESUrl = (path: string): string => `/#${hashPrefix}${path}`;

export const genJsAnchor = () => $('<a>').prop('href', 'javascript:void(0)');

export const genNavigateToRuqESUrl = (path: string) => () => {
  window.location.href = genRuqESUrl(path);
  window.location.reload();
};

export const createMainMenuButtonForDesktop = (text: string, url: string | null, cb: () => void | null, iconClasses: string) => {
  const icon = $('<i class="fa-width-rem text-left mr-3"></i>').addClass(iconClasses);
  return (url === null ? genJsAnchor() : $('<a>'))
    .prop('class', 'dropdown-item')
    .text(text)
    .prepend(icon)
    .on('click', cb)
    .prop('href', url || undefined)
    ;
};

export const createMainMenuButtonForMobile = (text: string, url: string | null, cb: () => void | null, iconClasses: string) => {
  const icon = $('<i class="fa-width-rem mr-3">').addClass(iconClasses);
  const link = (url === null ? genJsAnchor() : $('<a>'))
    .addClass('nav-link')
    .append(icon)
    .append(text)
    .on('click', cb)
    .click(() => $('.navbar-toggler').click())
    .prop('href', url || undefined)
  ;
  return $('<li class="nav-item">').append(link);
}

export const getDomain = (url: string): string => {
  const regex = /^https?:\/\/([\w\.]+)(?:\/.*)$/;
  if (!url) {
    return '';
  }
  return url.match(regex)[1];
}

export type ToolTipPosition =
  'top'
  | 'top-left'
  | 'top-right'
  | 'bottom'
  | 'bottom-left'
  | 'bottom-right'
  | 'left'
  | 'right';
export type ToolTipSize = 'small' | 'medium' | 'large' | 'fit';

export const withTooltip = (text: string, html: string) => (position: ToolTipPosition = 'bottom', size: ToolTipSize = 'fit'): string =>
  `<span role="tooltip" aria-label="${text}" data-microtip-position="${position}" data-microtip-size="${size}">${html}</span>`;

export const tryParseJSON: (x: string) => unknown | null = tryCatch(y => JSON.parse(y), () => null);

export const getElSize = (x: JQuery): [number, number] | null => {
  const w = $(x).width();
  const h = $(x).height();
  if (w === undefined || h === undefined) { return null; }
  return [w, h];
}
