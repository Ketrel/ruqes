import {pipe, update} from 'ramda';

export const isValidArrayIndex = (xs: unknown[]) => (i: number) => i >= 0 && i < xs.length

export const swapIndices = (i: number) => (j: number) => <T>(xs: T[]): T[] => {
  if (!isValidArrayIndex(xs)(i) || !isValidArrayIndex(xs)(j)) {
    return xs;
  }
  const iOldVal = xs[i];
  return pipe(
    update(i, xs[j]),
    update(j, iOldVal),
  )(xs);
};
