import {pipe, any, without, update} from 'ramda';

import {
  $c,
  $i,
  debugLog,
  genJsAnchor,
  isString,
  lastIndex,
  scriptVersion,
  setClass,
  showConfirmDialog,
  createMainMenuButtonForDesktop,
  createMainMenuButtonForMobile,
  withTooltip
} from 'common/common';
import {
  blurAnimatedCls,
  blurCls,
  btnDangerCls,
  btnPrimaryCls,
  clsPrefix,
  creatorMarkCls,
  creatorNameCls,
  formCls,
  settingsBackdropCls,
  settingsCaptionCls,
  settingsExpandoButtonActionCharacterCls,
  settingsLogoCls,
  settingsModalCls,
  settingsModalVisibleCls,
  widthAutoCls,
  settingsMenuCls,
  settingsActiveTabButtonCls,
  settingsExpandoOrdersCls,
  settingsPostRulesListCls,
  settingsPostRuleFormCls,
  settingsPostRulesNumberCls,
  settingsPostRuleListItemCls,
  settingsPostRuleListItemNameCls,
  settingsPostRuleBodyCls,
} from 'common/styles';
import {
  asExpandoButtonStyleOrDefault,
  asImageThumbnailIconTypeStyleOrDefault,
  Config,
  defaultConfig,
  expandoButtonStyleNames,
  expandoButtonStyles,
  imageThumbnailIconTypeStyleNames,
  imageThumbnailIconTypeStyles,
  PostRule,
  PostSave,
  readConfig,
  writeConfig
} from 'common/config';
import logoSvg from 'assets/logo.svg';
import {download} from 'common/downloader';
import {getTopLevelContainersForBlurring} from 'common/selectors';
import ChangeEvent = JQuery.ChangeEvent;
import {parsePostRule} from 'modules/post/parsePostRule';
import {swapIndices} from './swapIndices';

const genSettingsId = (x: string) => `${clsPrefix}${x}`;

const expandoButtonId = genSettingsId('expando');
const expandoButtonStyleId = genSettingsId('expando-style');
const expandoButtonAlignRightId = genSettingsId('expando-align-right');
const expandoButtonResizeId = genSettingsId('expando-resize');
const expandoButtonTextClosedId = genSettingsId('expando-text-closed');
const expandoButtonTextClosedListId = genSettingsId('expando-text-closed-list');
const expandoButtonTextOpenedId = genSettingsId('expando-text-opened');
const expandoButtonTextOpenedListId = genSettingsId('expando-text-opened-list');
const expandoButtonShowCommentsId = genSettingsId('expando-show-comments');
const expandoButtonPostTextOrderId = genSettingsId('expando-post-text-order');
const expandoButtonEmbedOrderId = genSettingsId('expando-post-embed-order');
const expandoButtonCommentsOrderId = genSettingsId('expando-post-comments-order');

const infiniteScrollId = genSettingsId('infinite-scroll');
const infiniteScrollLoadEarlierId = genSettingsId('infinite-scroll-load-earlier');

const bigVoteArrowsOnMobileId = genSettingsId('big-vote-arrows-on-mobile');
const voteEffectId = genSettingsId('vote-effect');

const openPostInNewTabId = genSettingsId('post-open-in-new-tab');
const hidePostsFromGuildsId = genSettingsId('post-hide-posts-from-guilds');
const fullyHidePostsFromGuildsId = genSettingsId('post-fully-hide-posts-from-guilds');
const hideAlreadyJoinedGuildsInDiscoveryId = genSettingsId('post-hide-already-joined-guilds');
const blurNsfwThumbnailsId = genSettingsId('post-blur-nsfw-thumbnails');
const createPostLoadTitleId = genSettingsId('create-post-load-title');
const createPostMarkdownPreviewId = genSettingsId('create-post-markdown-preview');
const operationsContainerId = genSettingsId('operations');
const importHelperId = genSettingsId('import-helper');
const imgBbKeyId = genSettingsId('img-bb-key');
const improvedTableStylesId = genSettingsId('improved-table-styles');
const lessAggressivePatronBadgesId = genSettingsId('less-aggressive-patron-badges');
const disablePostActionsJumpingAboveBarOnHoverId = genSettingsId('disable-post-actions-jumping-above-bar-on-hover');
const imageThumbnailIconTypeId = genSettingsId('image-thumbnail-icon-type');
const imageThumbnailIconTypeStyleId = genSettingsId('image-thumbnail-icon-type-style');
const imageThumbnailIconTypeBackgroundId = genSettingsId('image-thumbnail-icon-type-bg');
const closeButtonInImageDialogId = genSettingsId('close-button-in-image-dialog');
const postUpDownVotesAsTextId = genSettingsId('post-up-down-votes-as-text');
const savingEnabledId = genSettingsId('saving-enabled');

const commentCtrlEnterToSendId = genSettingsId('comment-ctrl-enter-to-send');
const commentBiggerFoldButtonsId = genSettingsId('comment-bigger-fold-buttons');
const commentUpDownVotesAsTextId = genSettingsId('comment-up-down-votes-as-text');
const commentPreviewId = genSettingsId('comment-preview');

const sidebarRightHideButtonId = genSettingsId('sidebar-right-hide-button');
const sidebarRightAutoHideId = genSettingsId('sidebar-right-auto-hide');
const sidebarIndependentScrollId = genSettingsId('sidebar-independent-scroll');

const postRulesEngineEnabledId = genSettingsId('post-rules-engine-enabled');
const postRuleSaveButtonId = genSettingsId('post-rule-save-button');
const postRulesNewButtonId = genSettingsId('post-rule-new-button');
const postRulesBackButtonId = genSettingsId('post-rule-back-button');
const postRuleBodyId = genSettingsId('post-rule-body');

const advancedEnabledId = genSettingsId('advanced-enabled-id');
const advancedJsId = genSettingsId('advanced-js');
const advancedCssId = genSettingsId('advanced-css');

const debugId = genSettingsId('debug');
const debugAutoOpenSettingsId = genSettingsId('debug-auto-open-settings');
const debugInsertTestPostRuleId = genSettingsId('debug-insert-test-post-rule');
const debugSectionId = genSettingsId('debug-section');
const debugForceUpgradeId = genSettingsId('debug-force-upgrade');

const moduleContainerCls = genSettingsId('module-container');
const moduleButtonCls = genSettingsId('module-button');

interface SettingsState {
  postRules: PostRule[];
  indexOfPostRuleBeingEdited: number | null;
  creatingNewRule: boolean;
  savedPosts: PostSave[];
}

const settingsState: SettingsState = {
  postRules: [],
  indexOfPostRuleBeingEdited: null,
  creatingNewRule: false,
  savedPosts: [],
};

const setVisibilityOfDebugSectionInSettings = (val: boolean) =>
  $i(debugSectionId).css({display: val ? 'block' : 'none'});

const initializeSettingsForm = (cfg: Config) => {
  const initChecked = (id: string, val: boolean) => $i(id).prop('checked', val);
  const initVal = (id: string, val: string | number) => $i(id).val(val);

  initChecked(expandoButtonId, cfg.expandoButton.enabled);
  initChecked(expandoButtonResizeId, cfg.expandoButton.resize);
  initVal(expandoButtonStyleId, cfg.expandoButton.style);
  initChecked(expandoButtonAlignRightId, cfg.expandoButton.alignRight);
  initVal(expandoButtonTextClosedId, cfg.expandoButton.textClosed);
  initVal(expandoButtonTextOpenedId, cfg.expandoButton.textOpened);
  initChecked(expandoButtonShowCommentsId, cfg.expandoButton.showComments);
  initVal(expandoButtonPostTextOrderId, cfg.expandoButton.postTextOrder);
  initVal(expandoButtonEmbedOrderId, cfg.expandoButton.embedOrder);
  initVal(expandoButtonCommentsOrderId, cfg.expandoButton.commentsOrder);

  initChecked(infiniteScrollId, cfg.infiniteScroll.enabled);
  initChecked(infiniteScrollLoadEarlierId, cfg.infiniteScroll.loadEarlier);
  initChecked(bigVoteArrowsOnMobileId, cfg.voting.bigVoteArrowsOnMobile);
  initChecked(voteEffectId, cfg.voting.clickEffect);
  initChecked(openPostInNewTabId, cfg.post.openInNewTab);
  initVal(hidePostsFromGuildsId, cfg.post.hidePostsFromGuilds);
  initChecked(fullyHidePostsFromGuildsId, cfg.post.fullyHidePostsFromGuilds);
  initChecked(hideAlreadyJoinedGuildsInDiscoveryId, cfg.post.hideAlreadyJoinedGuildsInDiscovery);
  initChecked(blurNsfwThumbnailsId, cfg.post.blurNsfwThumbnails);
  initChecked(createPostLoadTitleId, cfg.createPost.loadTitleButton);
  initChecked(createPostMarkdownPreviewId, cfg.createPost.markdownPreview);
  initVal(imgBbKeyId, cfg.external.imgbbKey);
  initChecked(improvedTableStylesId, cfg.post.improvedTableStyles);
  initChecked(lessAggressivePatronBadgesId, cfg.post.lessAggressivePatronBadges);
  initChecked(disablePostActionsJumpingAboveBarOnHoverId, cfg.post.disablePostActionsJumpingAboveBarOnHover);
  initChecked(imageThumbnailIconTypeId, cfg.post.imageThumbnailIconType);
  initVal(imageThumbnailIconTypeStyleId, cfg.post.imageThumbnailIconTypeStyle);
  initChecked(imageThumbnailIconTypeBackgroundId, cfg.post.imageThumbnailIconTypeBackground);
  initChecked(closeButtonInImageDialogId, cfg.post.closeButtonInImageDialog);
  initChecked(postUpDownVotesAsTextId, cfg.post.upDownVotesAsText);
  initChecked(savingEnabledId, cfg.save.enabled);

  initChecked(commentCtrlEnterToSendId, cfg.comment.ctrlEnterToSend);
  initChecked(commentBiggerFoldButtonsId, cfg.comment.ctrlEnterToSend);
  initChecked(commentUpDownVotesAsTextId, cfg.comment.upDownVotesAsText);
  initChecked(commentPreviewId, cfg.comment.preview);

  initChecked(sidebarRightHideButtonId, cfg.sidebar.rightButton);
  initChecked(sidebarRightAutoHideId, cfg.sidebar.autoHideRight);
  initChecked(sidebarIndependentScrollId, cfg.sidebar.independentScroll);

  initChecked(postRulesEngineEnabledId, cfg.post.rulesEngineEnabled);

  initChecked(advancedEnabledId, cfg.advanced.enabled);
  initVal(advancedJsId, cfg.advanced.js);
  initVal(advancedCssId, cfg.advanced.css);

  const debugEnabled = cfg.debug.enabled;
  initChecked(debugId, debugEnabled);
  initChecked(debugAutoOpenSettingsId, cfg.debug.autoOpenSettings);
  initChecked(debugInsertTestPostRuleId, cfg.debug.insertTestPostRule);
  setVisibilityOfDebugSectionInSettings(debugEnabled);

  settingsState.postRules = cfg.post.rules;
  refreshPostRules();
  refreshPostRulesElementsVisibility();

  settingsState.savedPosts = cfg.save.posts;
};

const openSettingsModal = async () => {
  getTopLevelContainersForBlurring().addClass(blurAnimatedCls).addClass(blurCls);
  initializeSettingsForm(await readConfig());
  $c(settingsBackdropCls).show();
  $c(settingsModalCls).addClass(settingsModalVisibleCls);
};

const closeSettingsModal = () => {
  getTopLevelContainersForBlurring().removeClass(blurCls);
  $c(settingsModalCls).removeClass(settingsModalVisibleCls);
  setTimeout(() => $c(settingsBackdropCls).hide(), 200); // wait for transition to finish
};

const getConfigFromSettingsFormAndState = (): Omit<Config, 'scriptVersion'> => {
  const getStrVal = (id: string) => $i(id).val() as string;
  const getNumVal = (id: string) => Number.parseInt(String($i(id).val())) ?? -1;
  const getChecked = (id: string) => Boolean($i(id).prop('checked'));
  return ({
    expandoButton: {
      enabled: getChecked(expandoButtonId),
      resize: getChecked(expandoButtonResizeId),
      style: pipe(getStrVal, asExpandoButtonStyleOrDefault)(expandoButtonStyleId),
      alignRight: getChecked(expandoButtonAlignRightId),
      textClosed: getStrVal(expandoButtonTextClosedId),
      textOpened: getStrVal(expandoButtonTextOpenedId),
      showComments: getChecked(expandoButtonShowCommentsId),
      postTextOrder: getNumVal(expandoButtonPostTextOrderId),
      embedOrder: getNumVal(expandoButtonEmbedOrderId),
      commentsOrder: getNumVal(expandoButtonCommentsOrderId),
    },
    infiniteScroll: {
      enabled: getChecked(infiniteScrollId),
      loadEarlier: getChecked(infiniteScrollLoadEarlierId),
    },
    voting: {
      bigVoteArrowsOnMobile: getChecked(bigVoteArrowsOnMobileId),
      clickEffect: getChecked(voteEffectId),
    },
    post: {
      openInNewTab: getChecked(openPostInNewTabId),
      hidePostsFromGuilds: getStrVal(hidePostsFromGuildsId),
      fullyHidePostsFromGuilds: getChecked(fullyHidePostsFromGuildsId),
      blurNsfwThumbnails: getChecked(blurNsfwThumbnailsId),
      hideAlreadyJoinedGuildsInDiscovery: getChecked(hideAlreadyJoinedGuildsInDiscoveryId),
      improvedTableStyles: getChecked(improvedTableStylesId),
      lessAggressivePatronBadges: getChecked(lessAggressivePatronBadgesId),
      disablePostActionsJumpingAboveBarOnHover: getChecked(disablePostActionsJumpingAboveBarOnHoverId),
      imageThumbnailIconType: getChecked(imageThumbnailIconTypeId),
      imageThumbnailIconTypeStyle: pipe(getStrVal, asImageThumbnailIconTypeStyleOrDefault)(imageThumbnailIconTypeStyleId),
      imageThumbnailIconTypeBackground: getChecked(imageThumbnailIconTypeBackgroundId),
      closeButtonInImageDialog: getChecked(closeButtonInImageDialogId),
      rulesEngineEnabled: getChecked(postRulesEngineEnabledId),
      rules: settingsState.postRules,
      upDownVotesAsText: getChecked(postUpDownVotesAsTextId),
    },
    sidebar: {
      rightButton: getChecked(sidebarRightHideButtonId),
      autoHideRight: getChecked(sidebarRightAutoHideId),
      independentScroll: getChecked(sidebarIndependentScrollId),
    },
    createPost: {
      loadTitleButton: getChecked(createPostLoadTitleId),
      markdownPreview: getChecked(createPostMarkdownPreviewId),
    },
    comment: {
      ctrlEnterToSend: getChecked(commentCtrlEnterToSendId),
      biggerFoldButtons: getChecked(commentBiggerFoldButtonsId),
      upDownVotesAsText: getChecked(commentUpDownVotesAsTextId),
      preview: getChecked(commentPreviewId),
    },
    external: {
      imgbbKey: getStrVal(imgBbKeyId),
    },
    debug: {
      enabled: getChecked(debugId),
      autoOpenSettings: getChecked(debugAutoOpenSettingsId),
      insertTestPostRule: getChecked(debugInsertTestPostRuleId),
    },
    advanced: {
      enabled: getChecked(advancedEnabledId),
      js: getStrVal(advancedJsId),
      css: getStrVal(advancedCssId),
    },
    save: {
      enabled: getChecked(savingEnabledId),
      posts: settingsState.savedPosts
    }
  });
};

const saveAndCloseSettingsModal = async () => {
  const newValues = {...await readConfig(), ...getConfigFromSettingsFormAndState()};
  debugLog('generated new config', newValues);
  await writeConfig(newValues);
  closeSettingsModal();
};

const saveAndReloadSettings = async () => {
  await saveAndCloseSettingsModal();
  window.location.reload();
};

const resetSettings = async () => {
  await writeConfig(defaultConfig);
  initializeSettingsForm(await readConfig());
}

const exportSettings = async () => {
  const cfg = await readConfig();
  download(JSON.stringify(cfg), 'application/json', 'RuqES-settings.json');
};

const importSettingsFileConfirmed = (evt: ChangeEvent) => {
  debugLog('importSettingsFileConfirmed', evt);
  const files = $i(importHelperId).prop('files');
  if (!files || !files[0]) {
    return;
  }
  const file = files[0];
  debugLog('got file', file);
  const reader = new FileReader();
  reader.onload = async (readEvt) => {
    const text = readEvt?.target?.result;
    debugLog('read result', text);
    let newCfg = null;
    try { newCfg = JSON.parse(text as any); } // 
    catch (e) { alert(`Failed to parse given file as JSON.`); }
    if (newCfg !== null) {
      await writeConfig(newCfg);
      window.location.reload();
    }
  }
  reader.readAsBinaryString(file);
};

const importSettings = async () => {
  $i(importHelperId).click();
};

const createSettingsDialog = () => {
  const mobileBadge = `<span class="badge badge-secondary">mobile</span>`;
  const desktopBadge = `<span class="badge badge-dark">desktop</span>`;
  const experimentalBadge = `<span class="badge badge-danger">experimental</span>`;
  const infiniteScrollCoolDownBadge = withTooltip('Adds cool-down to infinite scroll', `<span class="badge badge-info">IS CD</span>`);
  const deprecatedBadge = withTooltip('Will be removed in future version', `<span class="badge badge-warning">deprecated</span>`);
  const helpBadge = `<span class="badge badge-dark">?</span>`;
  const genSelectOptions = <T extends string>(values: readonly T[], valueToNameMap: Record<T, string>) =>
    values.map(x => `<option value="${x}">${valueToNameMap[x]}</option>`).join('\n');
  const expandoContent = `
<div class="form-group mb-2">
    <input type="checkbox" id="${expandoButtonId}" class="form-control" />
    <label for="${expandoButtonId}">Expando button enabled</label>
</div>
<div class="form-group mb-2">
    <label for="${expandoButtonStyleId}" class="mr-1">Style</label>
    <select id="${expandoButtonStyleId}" class="form-control">
        ${genSelectOptions(expandoButtonStyles, expandoButtonStyleNames)}
    </select>
</div>
<div class="form-group mb-2">
    <input type="checkbox" id="${expandoButtonShowCommentsId}" class="form-control" />
    <label for="${expandoButtonShowCommentsId}" class="mr-1">Show comments</label>${experimentalBadge}
</div>
<div class="form-group mb-2">
    <input type="checkbox" id="${expandoButtonResizeId}" class="form-control" />
    <label for="${expandoButtonResizeId}">Image resizing ${desktopBadge}</label>
</div>
<div class="form-group mb-2">
    <input type="checkbox" id="${expandoButtonAlignRightId}" class="form-control" />
    <label for="${expandoButtonAlignRightId}">Align right ${mobileBadge}</label>
</div>
<div class="form-group mb-2 ${settingsExpandoButtonActionCharacterCls}">
    <label for="${expandoButtonTextClosedId}" class="mr-1">Open character:</label><br />
    <input type="text" id="${expandoButtonTextClosedId}" class="form-control ${widthAutoCls}" maxlength="5" list="${expandoButtonTextClosedListId}" />
    <datalist id="${expandoButtonTextClosedListId}">
        <option value="+">
        <option value="☩">
        <option value="➕">
        <option value="ᚐ">
        <option value="⧾">
        <option value="⊕">
        <option value="≡">
        <option value="≡">
        <option value="𝍢">
        <option value="≣">
        <option value="𝍣">
        <option value="𝌉">
    </datalist>
</div>
<div class="form-group mb-2 ${settingsExpandoButtonActionCharacterCls}">
    <label for="${expandoButtonTextOpenedId}" class="mr-1">Close character:</label><br />
    <input type="text" id="${expandoButtonTextOpenedId}" class="form-control ${widthAutoCls}" maxlength="5" list="${expandoButtonTextOpenedListId}" />
        <datalist id="${expandoButtonTextOpenedListId}">
        <option value="-">
        <option value="–">
        <option value="―">
        <option value="⧿">
        <option value="⧿">
        <option value="➖">
        <option value="⊝">
        <option value="Θ">
    </datalist>
</div>

<hr>

<div class="${settingsExpandoOrdersCls} mb-1">
    Number values mean order/priority. The lower the number (relative to others) the higher the element will be shown.
</div>
<div class="ml-3 ${settingsExpandoOrdersCls}">
  <div class="form-group mb-2">
      <label for="${expandoButtonPostTextOrderId}">Post text</label>
      <input type="number" id="${expandoButtonPostTextOrderId}" class="form-control" />
  </div>
  <div class="form-group mb-2">
      <label for="${expandoButtonEmbedOrderId}">Embed</label>
      <input type="number" id="${expandoButtonEmbedOrderId}" class="form-control" />
  </div>
  <div class="form-group mb-2">
      <label for="${expandoButtonCommentsOrderId}">Comments</label>
      <input type="number" id="${expandoButtonCommentsOrderId}" class="form-control" />
  </div>
</div>
  `;
  const infiniteScrollContent = `
<div class="form-group mb-2">
    <input type="checkbox" id="${infiniteScrollId}" class="form-control" />
    <label for="${infiniteScrollId}">Infinite scroll</label>
</div>
<div class="form-group mb-2 ml-3">
    <input type="checkbox" id="${infiniteScrollLoadEarlierId}" class="form-control" />
    <label for="${infiniteScrollLoadEarlierId}" class="mr-1">Start loading next page earlier</label>${infiniteScrollCoolDownBadge('left')}
</div>
  `;
  const postContent = `
<div class="form-group mb-2">
    <input type="checkbox" id="${imageThumbnailIconTypeId}" class="form-control" />
    <label for="${imageThumbnailIconTypeId}">Show icon of thumbnail action</label>
</div>
<div class="form-group mb-2 ml-3">
    <label for="${imageThumbnailIconTypeStyleId}" class="mr-1">Style</label>
    <select id="${imageThumbnailIconTypeStyleId}" class="form-control">
        ${genSelectOptions(imageThumbnailIconTypeStyles, imageThumbnailIconTypeStyleNames)}
    </select>
</div>
<div class="form-group mb-2 ml-3">
    <input type="checkbox" id="${imageThumbnailIconTypeBackgroundId}" class="form-control" />
    <label for="${imageThumbnailIconTypeBackgroundId}">Dark background</label>
</div>


<div class="form-group mb-2">
    <input type="checkbox" id="${improvedTableStylesId}" class="form-control" />
    <label for="${improvedTableStylesId}">Improved styles for tables in posts</label>
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${openPostInNewTabId}" class="form-control" />
    <label for="${openPostInNewTabId}">Open posts in a new tab</label>
</div>

<div class="form-group mb-2 d-block">
    <label for="${hidePostsFromGuildsId}">Semi-hide posts from guilds:</label>
    ${deprecatedBadge()}
    <br />
    <input type="text" id="${hidePostsFromGuildsId}" class="form-control" placeholder="Guild names without '+' separated by comma ','" />
</div>
<div class="form-group mb-2 ml-3">
    <input type="checkbox" id="${fullyHidePostsFromGuildsId}" class="form-control" />
    <label for="${fullyHidePostsFromGuildsId}" class="mr-1">Fully hide</label>
    ${infiniteScrollCoolDownBadge()} &nbsp; ${deprecatedBadge()}
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${hideAlreadyJoinedGuildsInDiscoveryId}" class="form-control" />
    <label for="${hideAlreadyJoinedGuildsInDiscoveryId}" class="mr-1">Hide joined guilds on "discover" page</label> ${infiniteScrollCoolDownBadge('left')}
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${blurNsfwThumbnailsId}" class="form-control" />
    <label for="${blurNsfwThumbnailsId}">Blur thumbnails of NSFW posts</label> ${deprecatedBadge('left')}
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${lessAggressivePatronBadgesId}" class="form-control" />
    <label for="${lessAggressivePatronBadgesId}">Less aggressive patron badges</label>
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${disablePostActionsJumpingAboveBarOnHoverId}" class="form-control" />
    <label for="${disablePostActionsJumpingAboveBarOnHoverId}">Disable post actions jumping above bar on hover</label>
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${createPostLoadTitleId}" class="form-control" />
    <label for="${createPostLoadTitleId}">"Load title" button on create post page</label>
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${createPostMarkdownPreviewId}" class="form-control" />
    <label for="${createPostMarkdownPreviewId}">Post preview (Markdown)</label>
</div>

<div class="form-group mb-2 d-block">
    <label for="${imgBbKeyId}">
        <a href="https://api.imgbb.com/" target="_blank">imgbb</a> API key:
    </label><br />
    <input type="text" id="${imgBbKeyId}" class="form-control" />
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${closeButtonInImageDialogId}" class="form-control" />
    <label for="${closeButtonInImageDialogId}">Add close button to image dialog</label> ${mobileBadge}
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${postUpDownVotesAsTextId}" class="form-control" />
    <label for="${postUpDownVotesAsTextId}">Up-vote and down-vote counts as text</label> ${desktopBadge}
</div>
  `;
  const genModuleButton = (moduleName: string, label: string) =>
    `<a class="${moduleButtonCls} btn btn-secondary m-1" data-module="${moduleName}">${label}</a>`;
  const postRulesContent = `
    <div class="alert alert-danger" role="alert">
        Beware, this module is highly ${experimentalBadge}! It may be unstable and API may change at any moment.
    </div>
    <div class="form-group mb-2">
        <input type="checkbox" id="${postRulesEngineEnabledId}" class="form-control" />
        <label for="${postRulesEngineEnabledId}">Post rules engine enabled</label>
    </div>
    <div class="alert alert-danger" role="alert">
        ${withTooltip('While RuqES runs rules in a sandbox, the sandbox is not guaranteed to be perfect (nothing is).', helpBadge)('right', 'large')}
        It is recommended to only add rules of which you understand their code.
    </div>
    <div>
      <strong>Rules</strong> (<span class="${settingsPostRulesNumberCls}"></span>)
      <a class="btn btn-secondary my-1" id="${postRulesNewButtonId}" title="Create new rule">
            <i class="fad fa-cauldron"></i>
      </a>
    </div>
    <div class="${settingsPostRulesListCls} mb-2"></div>
    <div class="${settingsPostRuleFormCls}">
      <div class="form-group mb-1">
          <textarea id="${postRuleBodyId}" class="form-control text-monospace ${settingsPostRuleBodyCls}"></textarea>
      </div>
      <div class="d-flex justify-content-between">
        <a class="btn btn-secondary" id="${postRulesBackButtonId}">Back</a>
        <a class="btn btn-secondary" id="${postRuleSaveButtonId}"></a>
      </div>
    </div>
`;
  const advancedContent = `
<div class="alert alert-danger" role="alert">
    ${withTooltip('CSS can be a privacy problem (e.g. allowing tracking of your IP address), JS can be a grieve security issue (e.g. stealing security tokens from cookies or a password from a login form).', '<span class="badge badge-danger">Warning</span>')('right', 'large')}
    Only enable this module if you are absolutely sure about what you are doing. There are considerable privacy and security implications.<br>
    Do NOT mindlessly copy-paste code from untrusted sources.
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${advancedEnabledId}" class="form-control" />
    <label for="${advancedEnabledId}">Enable advanced module</label>
</div>

<div class="form-group mb-2 d-block">
    <label for="${advancedJsId}">Custom JavaScript:</label>
    <br />
    <textarea id="${advancedJsId}" class="form-control"></textarea>
</div>

<div class="form-group mb-2 d-block"> 
    <label for="${advancedCssId}">Custom CSS:</label>
    <br />
    <textarea id="${advancedCssId}" class="form-control"></textarea>
</div>
`;
  const commentContent = `
<div class="form-group mb-2">
    <input type="checkbox" id="${commentCtrlEnterToSendId}" class="form-control" />
    <label for="${commentCtrlEnterToSendId}"><kbd>CTRL</kbd> + <kbd>ENTER</kbd> to send comment</label>
</div>
<div class="form-group mb-2">
    <input type="checkbox" id="${commentBiggerFoldButtonsId}" class="form-control" />
    <label for="${commentBiggerFoldButtonsId}">Bigger fold buttons ${mobileBadge}</label>
</div>
<div class="form-group mb-2">
    <input type="checkbox" id="${commentUpDownVotesAsTextId}" class="form-control" />
    <label for="${commentUpDownVotesAsTextId}">Up-vote and down-vote counts as text</label>
    ${withTooltip(`Please note your vote isn't applied until page reload.`, helpBadge)('left', 'medium')}
</div>
<div class="form-group mb-2">
    <input type="checkbox" id="${commentPreviewId}" class="form-control" />
    <label for="${commentPreviewId}">Preview</label>
</div>
  `;
  const saveContent = `
<div class="form-group mb-2">
    <input type="checkbox" id="${savingEnabledId}" class="form-control" />
    <label for="${savingEnabledId}">Saving enabled</label> ${experimentalBadge}
</div>
`;
  const content = `
<hr class="mt-0 mb-2">
<form class="${formCls}">

<div class="${settingsMenuCls}">
  ${genModuleButton('expandoButton', 'Expando')}
  ${genModuleButton('infiniteScroll', 'Infinite scroll')}
  ${genModuleButton('voting', 'Voting')}
  ${genModuleButton('post', 'Post')}
  ${genModuleButton('postRules', 'Post rules')}
  ${genModuleButton('comment', 'Comment')}
  ${genModuleButton('save', 'Saving')}
  ${genModuleButton('sidebar', 'Sidebar')}
  ${genModuleButton('settings', 'Settings')}
  ${genModuleButton('advanced', 'Advanced')}
  ${genModuleButton('debug', 'Debug')}
</div>

<hr class="mt-1">

<div class="${moduleContainerCls}" data-module="expandoButton">
  ${expandoContent}
</div>

<div class="${moduleContainerCls}" data-module="infiniteScroll">
  ${infiniteScrollContent}
</div>

<div class="${moduleContainerCls}" data-module="voting">
  <div class="form-group mb-2">
      <input type="checkbox" id="${bigVoteArrowsOnMobileId}" class="form-control" />
      <label for="${bigVoteArrowsOnMobileId}">Bigger vote arrows ${mobileBadge}</label>
  </div>
  
  <div class="form-group mb-2">
      <input type="checkbox" id="${voteEffectId}" class="form-control" />
      <label for="${voteEffectId}">Vote effect</label>
  </div>
</div>

<div class="${moduleContainerCls}" data-module="post">
  ${postContent}
</div>

<div class="${moduleContainerCls}" data-module="postRules">
    ${postRulesContent}
</div>

<div class="${moduleContainerCls}" data-module="sidebar">
  <div class="form-group mb-2">
      <input type="checkbox" id="${sidebarRightHideButtonId}" class="form-control" />
      <label for="${sidebarRightHideButtonId}">Enable hide button for right sidebar</label>
  </div>
  <div class="form-group mb-2 ml-3">
      <input type="checkbox" id="${sidebarRightAutoHideId}" class="form-control" />
      <label for="${sidebarRightAutoHideId}">Auto-hide right sidebar</label>
  </div>
  <div class="form-group mb-2">
      <input type="checkbox" id="${sidebarIndependentScrollId}" class="form-control" />
      <label for="${sidebarIndependentScrollId}">Independent scroll</label>
      ${withTooltip('Doesn\'t move when scrolling main content.', helpBadge)()}
  </div>
</div>

<div class="${moduleContainerCls}" data-module="comment">
  ${commentContent}
</div>

<div class="${moduleContainerCls}" data-module="save">
  ${saveContent}
</div>

<div class="${moduleContainerCls}" data-module="advanced">
    ${advancedContent}
</div>

<div class="${moduleContainerCls}" data-module="debug">
  <div class="form-group mb-1">
      <input type="checkbox" id="${debugId}" class="form-control" />
      <label for="${debugId}">Debug mode</label>
  </div>
  <div id="${debugSectionId}" class="ml-3">
      <div class="form-group mb-2">
          <input type="checkbox" id="${debugAutoOpenSettingsId}" class="form-control" />
          <label for="${debugAutoOpenSettingsId}">Auto-open settings</label>
      </div>
      <a class="btn btn-primary ${btnPrimaryCls} mb-2" id="${debugForceUpgradeId}">Force config upgrade</a>
      <div class="form-group mb-2">
          <input type="checkbox" id="${debugInsertTestPostRuleId}" class="form-control" />
          <label for="${debugInsertTestPostRuleId}">Insert test post rule</label>
      </div>
  </div>
</div>

<div class="${moduleContainerCls}" data-module="settings">
  <div class="mb-1"><strong>Settings operations</strong></div>
  <div id="${operationsContainerId}" class="d-flex">
      <input id="${importHelperId}" type="file" class="d-none" accept="application/json">
  </div>
</div>

</form>
    `;
  const captionPart = `
<div class="d-flex justify-content-center align-items-baseline">
<div class="${settingsLogoCls}">${logoSvg}</div>
<h1 class="${settingsCaptionCls}">RuqES</h1>
<div class="ml-1" style="opacity: 0.5">
  <span class="position-absolute" style="top: 0.8em">${scriptVersion}</span>
  by <a href="https://ruqqus.com/@enefi">@enefi</a>
</div>
</div>
    `
  const contentEl = $(`<div>${content}</div>`);
  contentEl.find('form').submit(() => {
    saveAndReloadSettings();
    return false;
  });
  const cancelButton = $('<a class="btn btn-secondary">Cancel</a>').click(closeSettingsModal);
  const saveButton = $('<a class="btn btn-secondary">Save</a>').click(saveAndCloseSettingsModal);
  const saveAndReloadButton =
    $('<a class="btn btn-primary ml-2">Save & reload</a>').addClass(btnPrimaryCls).click(saveAndReloadSettings);
  const exportButton = $('<a class="btn btn-secondary">Export</a>').click(exportSettings);
  const importButton = $('<a class="btn btn-secondary ml-2">Import</a>').click(importSettings);
  const resetButton = $('<a class="btn btn-danger ml-2">Reset</a>').addClass(btnDangerCls).click(resetSettings);
  contentEl.find(`#${operationsContainerId}`)
    .append(exportButton)
    .append(importButton)
    .append(resetButton)
  ;
  contentEl.find(`#${importHelperId}`).change(importSettingsFileConfirmed);
  const changesWarningPart = `
<div class="flex-grow-1"></div>
<div>
  <hr class="my-2">
  <div>Changes will take effect after a page reload.</div>
  <hr class="my-2">
</div>
`;
  const modal = $('<div>')
    .addClass(settingsModalCls)
    .append(captionPart)
    .append(contentEl)
    .append(changesWarningPart)
    .append($('<div>').addClass(['d-flex', 'justify-content-between', 'mt-2'])
      .append(cancelButton)
      .append($('<div>')
        .append(saveButton)
        .append(saveAndReloadButton),
      ),
    )
  ;
  $i(debugForceUpgradeId, modal).click(() => readConfig({forceUpgrade: true}));
  $i(debugId, modal).change(x => setVisibilityOfDebugSectionInSettings((x.target as HTMLInputElement).checked));
  const backdrop = $('<div>')
    .addClass(settingsBackdropCls)
    .click(evt => {
      if ($(evt.target).hasClass(settingsBackdropCls)) {
        closeSettingsModal();
      }
    });
  $c(moduleButtonCls, modal).click(evt => {
    const el = $(evt.target);
    const buttonModuleName = el.data('module');
    el.parent().parent().find(`.${moduleContainerCls}`).each((_, rawEl) => {
      const containerEl = $(rawEl);
      const moduleName = containerEl.data('module');
      containerEl.toggle(moduleName === buttonModuleName);
    });
    el.parent().find(`.${moduleButtonCls}`).each((_, rawEl) => {
      const el = $(rawEl);
      setClass(el, settingsActiveTabButtonCls, el.data('module') === buttonModuleName);
    });
  });
  const defaultActiveTab = 0;
  $c(moduleContainerCls, modal).filter(i => i !== defaultActiveTab).hide();
  $c(moduleButtonCls, modal).filter(i => i === defaultActiveTab).addClass(settingsActiveTabButtonCls);
  $i(postRuleSaveButtonId, modal).click(onPostRuleSaveButtonClick);
  $i(postRulesNewButtonId, modal).click(() => {
    settingsState.creatingNewRule = true;
    refreshPostRulesElementsVisibility();
  });
  $i(postRulesBackButtonId, modal).click(() => {
    settingsState.creatingNewRule = false;
    settingsState.indexOfPostRuleBeingEdited = null;
    refreshPostRulesElementsVisibility();
  });
  return backdrop.append(modal).hide();
};

const onPostRuleDeleteClick = async (ruleName: string) => {
  const confirmed = await showConfirmDialog(`Really delete post rule "${ruleName}"?`);
  if (!confirmed) { return; }
  settingsState.postRules = settingsState.postRules.filter(x => x.name !== ruleName);
  refreshPostRules();
};

const onPostRuleMoveUpClick = (idx: number) => {
  settingsState.postRules = swapIndices(idx)(idx - 1)(settingsState.postRules);
  refreshPostRules();
}

const onPostRuleMoveDownClick = (idx: number) => {
  settingsState.postRules = swapIndices(idx)(idx + 1)(settingsState.postRules);
  refreshPostRules();
}

const onPostRuleEditClick = (idx: number) => {
  settingsState.indexOfPostRuleBeingEdited = idx;
  $i(postRuleBodyId).val(settingsState.postRules[idx].body);
  refreshPostRulesElementsVisibility();
};

const onPostRuleItemNameClick = (idx: number) => {
  const rule = settingsState.postRules[idx];
  rule.enabled = !rule.enabled;
  refreshPostRules();
};

const onPostRuleSaveButtonClick = () => {
  const bodyEl = $i(postRuleBodyId);
  const bodyText = bodyEl.val();
  debugLog('postRuleSaveButton', bodyText);
  const parsed = parsePostRule(String(bodyText));
  if (parsed.tag === 'ok') {
    const s = settingsState;
    const isEdit = s.indexOfPostRuleBeingEdited !== null;
    const rulesToCheck = isEdit ? without([s.postRules[s.indexOfPostRuleBeingEdited]])(s.postRules) : s.postRules;
    if (any((x: PostRule) => x.name === parsed.rule.name)(rulesToCheck)) {
      alert('Rule with same name already exists.');
    } else {
      if (isEdit) {
        s.postRules = update(s.indexOfPostRuleBeingEdited, parsed.rule, s.postRules)
      } else {
        s.postRules.push(parsed.rule);
      }
      bodyEl.val('');
      refreshPostRules();
      s.creatingNewRule = false;
      s.indexOfPostRuleBeingEdited = null;
      refreshPostRulesElementsVisibility();
    }
  } else {
    alert(parsed.error);
  }
};

const refreshPostRules = () => {
  $c(settingsPostRulesNumberCls).text(settingsState.postRules.length);
  const list = $c(settingsPostRulesListCls).empty();
  const createIcon = (classes: string): JQuery => $('<i>').addClass(classes);
  const createButton = (icon: JQuery | string, title: string): JQuery => {
    const processedIcon = isString(icon) ? createIcon(icon) : icon;
    return genJsAnchor().addClass('btn btn-secondary ml-1 py-1 px-2').html(processedIcon).prop('title', title);
  };
  const createItem = (rule: PostRule, idx: number): JQuery => {
    const removeButton = createButton('fas fa-skull', 'Delete rule').click(() => onPostRuleDeleteClick(rule.name));
    const editButton = createButton('fas fa-feather-alt', 'Edit').click(() => onPostRuleEditClick(idx));
    const upButton = createButton('fas fa-angle-up', 'Move up');
    if (idx === 0) {
      upButton.addClass('disabled');
    } else {
      upButton.click(() => onPostRuleMoveUpClick(idx));
    }
    const downButton = createButton('fas fa-angle-down', 'Move down');
    if (idx === lastIndex(settingsState.postRules)) {
      downButton.addClass('disabled');
    } else {
      downButton.click(() => onPostRuleMoveDownClick(idx));
    }
    const disabledIcon = createIcon('far fa-square');
    const enabledIcon = createIcon('far fa-check-square');
    const itemName = $('<div>')
      .addClass('d-inline-block text-truncate')
      .addClass(settingsPostRuleListItemNameCls)
      .text(rule.name)
      .prop('title', rule.body)
      .click(() => onPostRuleItemNameClick(idx))
      .prepend((rule.enabled ? enabledIcon : disabledIcon).addClass('mr-1'))
    ;
    return $('<div>')
      .addClass('py-1 pl-2 pr-1 d-flex align-items-center')
      .addClass(settingsPostRuleListItemCls)
      .append(itemName)
      .append(editButton)
      .append(upButton)
      .append(downButton)
      .append(removeButton)
      ;
  };
  settingsState.postRules.map(createItem).forEach(x => list.append(x));
};

const refreshPostRulesElementsVisibility = () => {
  debugLog('refreshPostRulesElementsVisibility', settingsState);
  const s = settingsState;
  const showForm: boolean = s.creatingNewRule || s.indexOfPostRuleBeingEdited !== null;
  $c(settingsPostRulesListCls).toggle(!showForm);
  $c(settingsPostRuleFormCls).toggle(showForm);
  $i(postRuleSaveButtonId).text(s.creatingNewRule ? 'Create rule' : 'Update rule');
  $i(postRulesNewButtonId).toggle(!showForm);
};

const setupCreatorMark = () => {
  const logoEl = $('<span>').html(logoSvg).addClass(creatorMarkCls).prop('title', 'RuqES developer');
  $c('user-name').filter((_, el) => $(el).text() === 'enefi').addClass(creatorNameCls).after(logoEl);
};

const createSettingsButtonForDesktop = () => createMainMenuButtonForDesktop('RuqES', null, openSettingsModal, 'fas fa-wrench');

const createSettingsButtonForMobile = () => createMainMenuButtonForMobile('RuqES', null, openSettingsModal, 'fas fa-wrench')

export const setupSettings = async () => {
  const cfg = await readConfig();
  debugLog('setupSettings', cfg);
  setupCreatorMark();
  $('#navbar #navbarResponsive > ul > li:last-child a[href="/settings"]').after(createSettingsButtonForDesktop());
  $('#navbarResponsive > ul:last-child a.nav-link[href="/settings"]').parent().after(createSettingsButtonForMobile());
  $('body').prepend(createSettingsDialog());
  if (cfg.debug.autoOpenSettings) { await openSettingsModal(); }
};
