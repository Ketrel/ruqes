import {pageLoadingCls} from 'common/styles';
import {$c, $i} from 'common/common';

export const getPosts = () =>
  $('#posts > .card, .posts > .card, #thread .card').filter((_, rawEl) => $(rawEl).closest('#GIFs').length === 0);
export const getPagination = (from?: JQuery) => $('ul.pagination', from).parent();
export const getNextPageLink = () => getPagination().find('a:contains(Next)');
export const getPageLoadingMarker = () => $c(pageLoadingCls);
export const getRightSidebarEl = () => $('.sidebar:not(.sidebar-left)');
export const getPostTitleInPostCreation = () => $i('post-title');
export const getPostUrlInPostCreation = () => $i('post-URL');
export const getTopLevelContainersForBlurring = () => $('#main-content-row, #submitform');
export const getPostListContainer = () => $i('main-content-col');
export const getComments = () => $('.comment-body .user-info').parent();
export const getPostActionsList = (from?: JQuery) => $('.post-actions > ul', from);
export const getPostActionsListOnDesktop = (from?: JQuery) =>
  getPostActionsList(from).filter((_, rawEl) => !$(rawEl).parent().parent().hasClass('d-md-none'));
export const getPostActionsListOnMobile = (from?: JQuery) =>
  getPostActionsList(from).filter((_, rawEl) => $(rawEl).parent().parent().hasClass('d-md-none'));
