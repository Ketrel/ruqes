import {includes, assoc, mergeDeepLeft, pipe} from 'ramda';

import {debugLog, enabledDebug, scriptVersion} from 'common/common';
import {DeepPartial} from '../types/DeepPartial';

const settingsConfigKey = 'config';

export const expandoButtonStyles = ['bigWide', 'big', 'medium', 'small'] as const;
export type ExpandoButtonStyle = typeof expandoButtonStyles[number];
export const expandoButtonStyleNames: Record<ExpandoButtonStyle, string> = {
  small: 'Small (icon)',
  medium: 'Medium',
  big: 'Big',
  bigWide: 'Big - extra wide'
}
export const defaultExpandoButtonStyle: ExpandoButtonStyle = 'big';
export const asExpandoButtonStyleOrDefault = (x: string): ExpandoButtonStyle =>
  includes(x, expandoButtonStyles) ? x as ExpandoButtonStyle : defaultExpandoButtonStyle;

export const imageThumbnailIconTypeStyles = ['emoji', 'fa'] as const;
export type ImageThumbnailIconTypeStyle = typeof imageThumbnailIconTypeStyles[number];
export const imageThumbnailIconTypeStyleNames: Record<ImageThumbnailIconTypeStyle, string> = {
  emoji: 'Emoji',
  fa: 'Font Awesome',
};
const defaultImageThumbnailIconTypeStyle: ImageThumbnailIconTypeStyle = 'fa';
export const asImageThumbnailIconTypeStyleOrDefault = (x: string): ImageThumbnailIconTypeStyle =>
  includes(x, imageThumbnailIconTypeStyles) ? x as ImageThumbnailIconTypeStyle : defaultImageThumbnailIconTypeStyle;

export interface PostRule {
  name: string;
  body: string;
  enabled: boolean;
}

export interface PostSave {
  title: string;
  author: string | null;
  guild: string;
  id: string;
  nsfw: boolean;
  url: string;
  link: string | null;
  thumbnail: string | null;
  date: string;
  dateRelative: string;
  dateRaw: string;
  previewModal: boolean;
}

export type Config = {
  scriptVersion: string,
  debug: {
    enabled: boolean,
    autoOpenSettings: boolean,
    insertTestPostRule: boolean,
  },
  expandoButton: {
    enabled: boolean,
    resize: boolean,
    style: ExpandoButtonStyle,
    alignRight: boolean,
    textClosed: string,
    textOpened: string,
    showComments: boolean,
    postTextOrder: number,
    embedOrder: number,
    commentsOrder: number,
  },
  infiniteScroll: {
    enabled: boolean,
    loadEarlier: boolean,
  },
  voting: {
    bigVoteArrowsOnMobile: boolean,
    clickEffect: boolean,
  },
  post: {
    openInNewTab: boolean,
    hidePostsFromGuilds: string,
    fullyHidePostsFromGuilds: boolean,
    blurNsfwThumbnails: boolean,
    hideAlreadyJoinedGuildsInDiscovery: boolean,
    improvedTableStyles: boolean,
    lessAggressivePatronBadges: boolean,
    disablePostActionsJumpingAboveBarOnHover: boolean,
    imageThumbnailIconType: boolean,
    imageThumbnailIconTypeStyle: ImageThumbnailIconTypeStyle,
    imageThumbnailIconTypeBackground: boolean,
    closeButtonInImageDialog: boolean,
    rulesEngineEnabled: boolean,
    rules: PostRule[],
    upDownVotesAsText: boolean,
  },
  createPost: {
    loadTitleButton: boolean,
    markdownPreview: boolean,
  },
  comment: {
    ctrlEnterToSend: boolean,
    biggerFoldButtons: boolean,
    upDownVotesAsText: boolean,
    preview: boolean,
  },
  sidebar: {
    rightButton: boolean,
    autoHideRight: boolean,
    independentScroll: boolean,
  },
  external: {
    imgbbKey: string,
  },
  advanced: {
    enabled: boolean,
    js: string,
    css: string,
  },
  save: {
    enabled: boolean,
    posts: PostSave[],
  }
}

export type PartialConfig = DeepPartial<Config>;

export const defaultConfig: Config = Object.freeze({
  scriptVersion,
  debug: {
    enabled: false,
    autoOpenSettings: false,
    insertTestPostRule: false,
  },
  expandoButton: {
    enabled: true,
    resize: true,
    style: defaultExpandoButtonStyle,
    alignRight: false,
    textClosed: '+',
    textOpened: '-',
    showComments: true,
    postTextOrder: 1,
    embedOrder: 2,
    commentsOrder: 3,
  },
  infiniteScroll: {
    enabled: true,
    loadEarlier: false,
  },
  voting: {
    bigVoteArrowsOnMobile: true,
    clickEffect: true,
  },
  post: {
    openInNewTab: false,
    hidePostsFromGuilds: '',
    fullyHidePostsFromGuilds: false,
    blurNsfwThumbnails: false,
    hideAlreadyJoinedGuildsInDiscovery: true,
    improvedTableStyles: true,
    lessAggressivePatronBadges: false,
    disablePostActionsJumpingAboveBarOnHover: false,
    imageThumbnailIconType: true,
    imageThumbnailIconTypeStyle: defaultImageThumbnailIconTypeStyle,
    imageThumbnailIconTypeBackground: false,
    closeButtonInImageDialog: true,
    rulesEngineEnabled: false,
    rules: [],
    upDownVotesAsText: true,
  },
  createPost: {
    loadTitleButton: true,
    markdownPreview: true,
  },
  comment: {
    ctrlEnterToSend: true,
    biggerFoldButtons: true,
    upDownVotesAsText: true,
    preview: true,
  },
  sidebar: {
    rightButton: true,
    autoHideRight: false,
    independentScroll: true,
  },
  external: {
    imgbbKey: '',
  },
  advanced: {
    enabled: false,
    js: '',
    css: '',
  },
  save: {
    enabled: true,
    posts: [],
  }
});

const upgradeConfig = (cfg: PartialConfig): Config => {
  return pipe(
    mergeDeepLeft(cfg),
    assoc('scriptVersion', scriptVersion),
  )(defaultConfig) as Config;
};

interface ReadConfigArgs {
  forceUpgrade?: boolean;
}

export const readConfig = async ({forceUpgrade}: ReadConfigArgs = {}): Promise<Config> => {
  const rawCfg = await GM.getValue(settingsConfigKey, defaultConfig) as PartialConfig;
  let cfg: Config;
  if (rawCfg.scriptVersion !== scriptVersion || forceUpgrade) {
    console.log(`[RuqES] Upgrading config ${rawCfg.scriptVersion} -> ${scriptVersion}`);
    const upgradedConfig = upgradeConfig(rawCfg);
    cfg = upgradedConfig;
    await writeConfig(upgradedConfig);
  } else {
    cfg = rawCfg as Config;
  }
  debugLog('readConfig', cfg);
  if (cfg.debug.enabled) { enabledDebug(); }
  return cfg;
};

export const writeConfig = async (x: Config) =>
  await GM.setValue(settingsConfigKey, x as any); // value can be an object, types are incorrect
