import microtip from 'microtip/microtip.css';

import {addStyle} from 'common/domUtils';
import logoSvg from 'assets/logo.svg';

export const clsPrefix = 'RuqES-by-enefi--';
export const genClsName = (x: string) => clsPrefix + x;
export const expandBoxCls = genClsName('box');
export const expandBoxOpenedCls = genClsName('box-opened');
export const boxEmptyCls = genClsName('box-empty');
export const expandoBtnCls = genClsName('expando-button');
export const expandoStylesCls = genClsName('expando-styles');
export const boxImgCls = genClsName('box-img');
export const boxGuildCls = genClsName('box-guild');
export const boxEmbedCls = genClsName('box-embed');
export const boxPostTextCls = genClsName('box-post-text');
export const boxPostCommentsCls = genClsName('box-post-comments');
export const boxPostCommentsWithButtonCls = genClsName('box-post-comments-with-button');
export const pageLoadingCls = genClsName('page-loader');
export const postLoadingCls = genClsName('post-loader');
export const loadingDotsCls = genClsName('loading-dots');
export const codeBlockCls = genClsName('code-block');
export const textLoaderCls = genClsName('text-loader');
export const errorTextCls = genClsName('error-text');
export const pageLoadingErrorCls = genClsName('page-loading-error');
export const ruqesMarkCls = genClsName('ruqes-mark');
export const creatorMarkCls = genClsName('creator-mark');
export const creatorNameCls = genClsName('creator-name');
export const settingsBackdropCls = genClsName('settings-backdrop');
export const settingsModalCls = genClsName('settings-modal');
export const settingsModalVisibleCls = genClsName('settings-modal-shown');
export const settingsCaptionCls = genClsName('settings-caption');
export const settingsLogoCls = genClsName('settings-logo');
export const settingsExpandoButtonActionCharacterCls = genClsName('settings-expando-action-char');
export const settingsMenuCls = genClsName('settings-menu');
export const settingsActiveTabButtonCls = genClsName('settings-active-tab-button');
export const settingsExpandoOrdersCls = genClsName('settings-expando-orders');
export const settingsPostRulesListCls = genClsName('settings-post-rules-list');
export const settingsPostRuleListItemCls = genClsName('settings-post-rule-list-item');
export const settingsPostRuleListItemNameCls = genClsName('settings-post-rule-list-item-name');
export const settingsPostRuleFormCls = genClsName('settings-post-rule-form');
export const settingsPostRuleBodyCls = genClsName('settings-post-rule-body');
export const settingsPostRulesNumberCls = genClsName('settings-post-rules-number');
export const formCls = genClsName('form');
export const blurAnimatedCls = genClsName('blur-anim');
export const blurCls = genClsName('blur');
export const voteAnimCls = genClsName('vote-anim');
export const voteAnimHandlerAppliedCls = genClsName('vote-anim-handler-applied');
export const bigVoteArrowsOnMobileCls = genClsName('big-vote-arrows-on-mobile');
export const semiHiddenPostCls = genClsName('semi-hidden-post');
export const sidebarButtonCls = genClsName('sidebar-button');
export const sidebarButtonIconCls = genClsName('sidebar-button-icon');
export const sidebarClosedCls = genClsName('sidebar-closed');
export const sidebarIndependentScrollCls = genClsName('sidebar-independent-scroll');
export const hideScrollbarCls = genClsName('hide-scrollbar');
export const btnPrimaryCls = genClsName('btn-primary');
export const btnDangerCls = genClsName('btn-danger');
export const loadTitleButtonCls = genClsName('post-load-title-button');
export const loadTitleButtonLoadingCls = genClsName('post-load-title-button-loading');
export const uploadImageToImgbbButtonCls = genClsName('post-upload-imgbb-button');
export const uploadImageToImgbbButtonHighlightedCls = genClsName('post-upload-imgbb-button-highlighted');
export const uploadImageToImgbbButtonLoadingCls = genClsName('post-upload-imgbb-button-loading');
export const postPreviewCls = genClsName('post-preview');
export const postPreviewButtonCls = genClsName('post-preview-button');
export const blurNsfwThumbnailCls = genClsName('blur-nsfw-thumbnail');
export const blurThumbnailCls = genClsName('blur-thumbnail');
export const widthAutoCls = genClsName('width-auto');
export const infiniteScrollCoolDownMarkerCls = genClsName('inifinite-scroll-cooldown-marker');
export const improvedTableCls = genClsName('improved-table');
export const nonAggressiveCls = genClsName('non-aggressive');
export const disablePostActionsJumpingAboveBarOnHoverCls = genClsName('disable-post-actions-jumping-above-bar-on-hover');
export const resizableDirectImageCls = genClsName('resizable-direct-image');
export const imageThumbnailTypeIconCls = genClsName('image-thumbnail-type-icon');
export const imageThumbnailTypeIconBackgroundCls = genClsName('image-thumbnail-type-icon-bg');
export const imageThumbnailTypeIconOnMobileCls = genClsName('image-thumbnail-type-icon-on-mobile');
export const imageThumbnailTypeIconFACls = genClsName('image-thumbnail-type-icon-fa');
export const closeButtonInImageDialogCls = genClsName('close-button-in-image-dialog');
export const postRulesAppliedCls = genClsName('post-rules-applied');
export const advancedModuleJavaScriptId = genClsName('advanced-module-java-script');
export const advancedModuleCssId = genClsName('advanced-module-css');
export const commentBiggerFoldButtonsCls = genClsName('comment-bigger-fold-buttons');
export const commentUpDownVotesAsTextProcessedCls = genClsName('comment-up-down-votes-as-text-processed');
export const commentPreviewCls = genClsName('comment-preview');
export const commentPreviewAppliedCls = genClsName('comment-preview-applied');
export const upDownVotesAsTextUpCls = genClsName('up-down-votes-as-text-up');
export const UpDownVotesAsTextDownCls = genClsName('up-down-votes-as-text-down');
export const upDownVotesAsTextWrapperCls = genClsName('up-down-votes-as-text-wrapper');
export const postUpDownVotesAsTextProcessedCls = genClsName('post-up-down-votes-as-text-processed');
export const savePostProcessedCls = genClsName('save-post-processed');
export const saveGuildImageCls = genClsName('save-guild-image');
export const saveEmptySavesButtonPlaceholderCls = genClsName('save-empty-saves-button-placeholder');

export const setupStyles = (isDarkTheme: boolean) => {
  const primaryColor = 'rgba(128, 90, 213, 1)';
  const ruqesColor = '#800080';
  const containerBorderColor = isDarkTheme ? '#303030' : '#E2E8F0';
  const containerBackgroundColor = isDarkTheme ? '#181818' : '#D2D8E0';
  const containerBackgroundColorSecondary = isDarkTheme ? '#232323' : '#c8ced6';
  const textColor = isDarkTheme ? 'white' : 'black';
  const revTextColor = isDarkTheme ? 'black' : 'white';
  const arrowVoteColor = isDarkTheme ? '#303030' : '#cbd5e0';
  const upvoteColor = '#805ad5';
  const downvoteColor = '#38b2ac';
  const anchorColorInDarkTheme = '#cfcfcf';
  const anchorColorInLightTheme = '#121213';
  const anchorColor = isDarkTheme ? anchorColorInDarkTheme : anchorColorInLightTheme;
  addStyle(microtip);
  addStyle(`
.${widthAutoCls} { width: auto; }
  
.${expandoBtnCls} {
  margin: -0.4em 1em -0.4em 0;
  padding: 0;
  display: flex;
  align-items: center;
  justify-content: center;
}

.${expandoBtnCls}:last-child {
  margin: -0.4em 0 -0.4em 1em;
}

.${expandoBtnCls}--style-bigWide {
  width: 3.25em;
  height: 2.5em;
}
.${expandoBtnCls}--style-big {
  width: 2.5em;
  height: 2.5em;
}
.${expandoBtnCls}--style-medium {
  width: 2em;
  height: 2em;
  font-size: 90%;
}
.${expandoBtnCls}--style-small {
  width: 1.5em;
  height: auto;
  margin: 0 0.5em 0 0;
  border: 0;
  background: none;
}

.${expandBoxCls} {
  display: none;
  max-width: 100%;
  border: 1px solid ${containerBorderColor};
  margin-top: 1em;
  padding-left: 0;
  padding-right: 0;
  word-break: break-word;
}

.${expandBoxOpenedCls} {
  display: flex;
  flex-direction: column;
}

img.${boxImgCls} {}

.stretched-link::after { display: none; }

.${boxEmbedCls} {
  border-left: 1px solid ${containerBorderColor};
}

.${boxPostTextCls} {
  border-left: 1px solid ${containerBorderColor};
  padding: 5px;
}

.${boxPostTextCls} p:last-child {
  margin-bottom: 0;
}

.${boxPostCommentsCls} {
  padding: 5px;
}

.${boxPostCommentsCls} .comment:first-child {
  margin-top: 0.5em;
}
.${boxPostCommentsWithButtonCls} {
  padding: 5px;
}

.${boxGuildCls} {
  background-color: ${containerBorderColor};
  border-radius: 0.25rem;
  font-size: 135%;
  margin: 0.5em;
  transition: background-color 0.1s, box-shadow 0.1s; 
}
.${boxGuildCls}:hover {
  background-color: ${containerBackgroundColorSecondary};
  box-shadow: ${revTextColor} inset 0px 0px 2px;
}
.${boxGuildCls} img {
  border-radius: 50%;
  width: 2.5em;
}

@keyframes ruqesLoadingDots {
  0% {
    transform: translate(0, 0) scale(1);
    color: ${primaryColor}
  }
  25% {
    transform: translate(0, -0.2em) scale(1.33);
  }
  50% {
    transform: translate(0, 0) scale(1);
  }
  55% {
  }
  65% {
    color: ${textColor}
  }
}

.${loadingDotsCls} i {
  animation-name: ruqesLoadingDots;
  animation-duration: 2s;
  animation-delay: 0s;
  animation-iteration-count: infinite;
  animation-timing-function: ease-in-out;
  display: inline-block;
}

.${loadingDotsCls} i:nth-child(2) { animation-delay: 0.2s; }
.${loadingDotsCls} i:nth-child(3) { animation-delay: 0.4s; }

.${codeBlockCls} {
  border: 1px solid ${containerBorderColor};
  padding: 1em;
  font-size: 66%;
  background-color: lightgray;
}

.${textLoaderCls} {
  font-weight: bold;
}

.${pageLoadingCls} {
  margin: 1em 0 3em 0;
  text-align: center;
}

.${errorTextCls} {
  font-weight: bold;
  color: red;
}

.${pageLoadingErrorCls} {
  text-align: left;
}

.${ruqesMarkCls} svg {
  width: 1.2em;
  height: 1.2em;
}

.${creatorMarkCls} svg {
  width: 1.2em;
  height: 1.2em;
}

.${creatorNameCls} {
  transition: color 0.2s;
  color: ${isDarkTheme ? '#bf40bf' : '#800080'};
}
.${creatorNameCls}:hover {
  color: ${textColor};
}

.${settingsBackdropCls} {
  position: fixed;
  width: 100%;
  height: 100%;
  background-color: ${`${containerBackgroundColor}66`};
  z-index: 10000;
  top: 0;
}

.${settingsModalCls}::-webkit-scrollbar {
    height: 5px;
    width: 5px;
    background: ${containerBorderColor};
}
.${settingsModalCls}::-webkit-scrollbar-thumb {
    background: ${containerBackgroundColor};
}
.${settingsModalCls} {
  position: fixed;
  left: 50%;
  top: 100px;
  background-color: ${containerBorderColor};
  border: 2px solid ${containerBackgroundColor};
  border-radius: 5px;
  padding: 0.5em 1em 0.8em 1em;
  transform: translate(-50%, 0%) scale(0.8);
  min-width: 420px;
  max-width: 36em;
  overflow-y: auto;
  max-height: calc(100% - 110px);
  opacity: 0;
  transition: transform 0.2s, opacity 0.1s;
  scrollbar-color: ${containerBackgroundColor} ${containerBorderColor};
  scrollbar-width: thin;
  display: flex;
  flex-direction: column;
}
.${settingsModalVisibleCls} {
  transform: translate(-50%, 0%) scale(1);
  opacity: 1;
}
@media (max-width: 575.98px) {
  .${settingsModalCls} {
    width: 100%;
    min-height: 100%;
    min-width: auto;
    max-width: 100%;
    top: 0;
  }
}

.${formCls} input[type="checkbox"] {
  width: auto;
  height: auto;
  display: inline-block;
  margin-right: 0.3em;
}

.${formCls} label {
  margin-bottom: 0;
}

.${formCls} .form-group {
  margin-bottom: 0;
  display: flex;
  align-items: center;
}

.${settingsMenuCls} {
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
}

.${settingsActiveTabButtonCls} {
  border-color: ${ruqesColor} !important;
}

.${settingsExpandoOrdersCls} {
}
.${settingsExpandoOrdersCls} input {
  width: 5em;
}
.${settingsExpandoOrdersCls} label {
  width: 7em;
}

.${settingsPostRuleListItemCls} {}
.${settingsPostRuleListItemCls}:nth-child(even) {
  background-color: rgba(0, 0, 0, 0.1);
}

.${settingsPostRuleListItemNameCls} { 
  width: calc(100% - 8em);
  cursor: pointer;
}

.${settingsPostRuleBodyCls} {
  min-height: 8em;
}

.${blurAnimatedCls} {
  filter: blur(0);
  transition: filter 0.5s ease-in-out;
}

.${blurCls} {
  filter: blur(2px);
}

.${settingsCaptionCls} {
  text-shadow: ${isDarkTheme ? 'black' : '#999'} 1px 0.5px 0px;
}

.${settingsLogoCls} {
  display: flex;
}
.${settingsLogoCls} svg {
  width: 2em;
  height: 2em;
  margin-right: 0.2em;
}

.${settingsExpandoButtonActionCharacterCls} input {
  width: 4em;
}
.${settingsExpandoButtonActionCharacterCls} label {
  width: 10em;
}

@keyframes ruqesVoteEffect {
  0% {
    transform: scale(1);
    opacity: 0;
  }
  1% {
    opacity: 0.8;
  }
  90% {
    transform: scale(3);
  }
  100% {
    transform: scale(3.1);
    opacity: 0;
  }
}

.${voteAnimCls} {
  animation-name: ruqesVoteEffect;
  animation-duration: 0.75s;
  animation-delay: 0s;
  animation-iteration-count: 1;
  animation-timing-function: ease-in-out;
  position: absolute !important;
  left: 0%;
  top: 20%;
}
.${voteAnimCls}.fa-arrow-alt-up {
  color: ${upvoteColor} !important;
}
.${voteAnimCls}.fa-arrow-alt-down {
  color: ${downvoteColor} !important;
}

.${bigVoteArrowsOnMobileCls} .${voteAnimCls} {
  top: 0%;
}

#frontpage .voting.${bigVoteArrowsOnMobileCls},
#search .voting.${bigVoteArrowsOnMobileCls},
#userpage .voting.${bigVoteArrowsOnMobileCls},
#guild .voting.${bigVoteArrowsOnMobileCls}
{
  margin-bottom: -1rem;
  margin-top: -0.4rem;
}

.${bigVoteArrowsOnMobileCls} .arrow-up::before,
.${bigVoteArrowsOnMobileCls} .arrow-down::before,
.${bigVoteArrowsOnMobileCls} .arrow-up:hover::before,
.${bigVoteArrowsOnMobileCls} .arrow-down:hover::before
{
  font-size: 1.5rem;
}

.${semiHiddenPostCls} {
  opacity: 0.2;
  transition: opacity 0.33s;
  max-height: 1.5em;
  background-color: ${containerBackgroundColor};
  overflow: hidden;
  padding-top: 0 !important;
}
.${semiHiddenPostCls}:hover {
  opacity: 1;
  max-height: none;
}

.${sidebarButtonCls} {
  paddingTop: 0;
  position: fixed;
}

.${sidebarButtonCls}.opened {
  position: relative;
}

.${sidebarButtonIconCls}::before {
    cursor: pointer;
    font-size: 1rem;
    color: #777;
    font-family: "Font Awesome 5 Pro";
    font-weight: 900;
    content: "";
}

.${sidebarButtonCls}.opened .${sidebarButtonIconCls}::before {
    cursor: pointer;
    font-size: 1rem;
    color: #777;
    font-family: "Font Awesome 5 Pro";
    font-weight: 900;
    content: "";
}

.${sidebarClosedCls} {
    max-width: 3em;
    padding: 0;
}
.${sidebarClosedCls} > :not(:first-child) {
    display: none !important;
}
.${sidebarClosedCls} > .${sidebarIndependentScrollCls} > :not(:first-child) {
    display: none !important;
}

.${sidebarIndependentScrollCls} {
  position: fixed;
  overflow-y: auto;
  overflow-x: hidden;
  height: calc(100vh - 49px);
  padding-right: 15px;
}

.${hideScrollbarCls} {
  scrollbar-width: none;
  -ms-overflow-style: none;
}
.${hideScrollbarCls}::-webkit-scrollbar {
  width: 0px;
}

.${btnPrimaryCls} {
    color: rgb(207, 207, 207) !important;
    background-color: #6133c9;
    border-color: #5c31bf;
}

.${btnPrimaryCls}:not(:disabled):not(.disabled):active, .${btnPrimaryCls}:not(:disabled):not(.disabled).active, .show>.${btnPrimaryCls}.dropdown-toggle {
    color: #fff !important;
    background-color: #6133c9;
    border-color: #5c31bf;
}

.${btnDangerCls} {
    color: rgb(207, 207, 207) !important;
    background-color: #e53e3e;
    border-color: #e53e3e;
}

@keyframes ruqesSpinning {
  0% {
    transform: scale(1) rotate(0deg);
  }
  30% {
    transform: scale(1.2) rotate(108deg);
  }
  60% {
    transform: scale(0.8) rotate(216deg);
  }
  100% {
    transform: scale(1) rotate(360deg);
  }
}

.${loadTitleButtonCls} {
  margin-top: -0.4em;
}
.${loadTitleButtonCls} svg {
  width: 1.2em;
  height: 1.2em;
  margin-left: -0.3em;
  margin-right: 0.2em;
}
.${loadTitleButtonCls}.${loadTitleButtonLoadingCls} svg {
  animation: 1s infinite linear ruqesSpinning;
}

.${uploadImageToImgbbButtonCls} {
  margin-top: -0.4em;
  border: 1px dashed ${ruqesColor};
}
.${uploadImageToImgbbButtonCls} svg {
  width: 1.2em;
  height: 1.2em;
  margin-left: -0.3em;
  margin-right: 0.2em;
}
.${uploadImageToImgbbButtonCls}.${uploadImageToImgbbButtonLoadingCls} svg {
  animation: 1s infinite linear ruqesSpinning;
}

.${loadTitleButtonCls} ~ .${uploadImageToImgbbButtonCls} {
  margin-left: 0.5em;
}

.${uploadImageToImgbbButtonHighlightedCls} {
  border-style: solid;
}

body.${blurNsfwThumbnailCls} .card.nsfw .post-img {
  filter: blur(7px) saturate(0.3);
}

.${blurThumbnailCls} .post-img {
  filter: blur(7px) saturate(0.3);
} 

.${infiniteScrollCoolDownMarkerCls} {
  font-size: 130%;
  margin-left: 1em;
}

.${improvedTableCls} td, .${improvedTableCls} th {
  border: 1px solid ${containerBorderColor};
  padding: 0.2em 0.5em;
}
.${improvedTableCls} th {
  background-color: ${containerBackgroundColorSecondary};
}

.${nonAggressiveCls} {
  opacity: 0.33;
  transition: opacity 0.2s;
}
.${nonAggressiveCls}:hover {
  opacity: 1;
}

.${disablePostActionsJumpingAboveBarOnHoverCls} .post-actions:hover,
.${disablePostActionsJumpingAboveBarOnHoverCls} .post-actions:focus {
    z-index: 3;
}

.${resizableDirectImageCls} {
  cursor: nwse-resize;
}

.${imageThumbnailTypeIconCls} {
  position: absolute;
  right: 1px;
  top: 49px;
  width: 20px;
  height: 20px;
  text-align: center;
  border-radius: 5px;
  pointer-events: none;
  opacity: 0.9;
  color: white;
  text-shadow: black 1px 0 0, black 0 1px 0, black -1px 0 0, black 0 -1px 0;
  z-index: 2; /* otherwise broken in FF */
}
.${imageThumbnailTypeIconBackgroundCls} {
  background: #00000033;
}
.${imageThumbnailTypeIconOnMobileCls} {
  right: 16px;
  top: 46px;
}
.${imageThumbnailTypeIconFACls} {
  color: white;
}

.${postPreviewCls} {
  border: 1px solid ${containerBorderColor};
  padding: 1em;
  background-color: ${containerBackgroundColor};
}
.${postPreviewButtonCls} {
  color: ${anchorColor};
  text-decoration: none;
}

.${commentPreviewCls} {
  position: relative;
  border: 1px solid ${containerBorderColor};
  padding: 0.5em;
  background-color: ${containerBackgroundColor};
  z-index: 0;
}

:not(.collapsed) > .${commentPreviewCls} {
  display: none;
}

.${commentPreviewCls}:after {
  content: "Comment preview";
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  padding: 0.5em;
  background-image: url("data:image/svg+xml;base64,${btoa(logoSvg)}");
  background-size: auto 75%;
  background-repeat: no-repeat;
  background-position: center;
  opacity: 0.05;
  overflow: hidden;
  text-align: right;
  z-index: -1;
}

@media (max-width: 767.98px) {
  .${commentBiggerFoldButtonsCls} .comment .comment-collapse::before {
    font-size: 15px;
  }
  .${commentBiggerFoldButtonsCls} .comment.collapsed .comment-collapse:hover::before {}
}

.${commentUpDownVotesAsTextProcessedCls} {}
$.{upDownVotesAsTextWrapperCls} {}
.${upDownVotesAsTextUpCls} {
  color: ${upvoteColor};
}
.${UpDownVotesAsTextDownCls} {
  color: ${downvoteColor};
}
.${upDownVotesAsTextUpCls}, .${UpDownVotesAsTextDownCls} {}

.${saveEmptySavesButtonPlaceholderCls} a {
  color: ${textColor};
}
`);
};
