import {addCustomScript} from 'common/domUtils';

const emitEvent = (name: string, data?: object) => {
  const evt = new CustomEvent(`ruqes${name}`, {bubbles: true, detail: data});
  document.dispatchEvent(evt);
}

export const emitInfiniteScrollLoaded = (pageNumber: number, url: string, pageElement: HTMLElement) => {
  emitEvent('infinitescrollload', {pageNumber, url, pageElement});
};

export const emitInfiniteScrollLoadStart = (pageNumber: number, url: string) => {
  emitEvent('infinitescrollloadstart', {pageNumber, url});
}

export const emitInfiniteScrollLoadFail = (url: string, status: number, statusText: string) => {
  emitEvent('infinitescrollloaderror', {url, status, statusText});
}

export const setupDebugEventListeners = () => {
  addCustomScript(`
document.addEventListener('ruqesinfinitescrollload', evt => console.log('DOC got infi scroll LOAD event', evt));
window.addEventListener('ruqesinfinitescrollload', evt => console.log('WIN got infi scroll LOAD event', evt));

document.addEventListener('ruqesinfinitescrollloadstart', evt => console.log('DOC got infi scroll START event', evt));
document.addEventListener('ruqesinfinitescrollloaderror', evt => console.log('DOC got infi scroll ERROR event', evt));
`);
}
