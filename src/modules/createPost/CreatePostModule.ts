import {path, prop} from 'ramda';
import debounce from 'lodash.debounce';

import {Config} from 'common/config';
import {debugLog, preventDefaults, xhr, $i, createRuqesMark, genJsAnchor} from 'common/common';
import logoSvg from 'assets/logo.svg';
import {
  improvedTableCls,
  loadTitleButtonCls,
  loadTitleButtonLoadingCls,
  nonAggressiveCls,
  postPreviewButtonCls,
  postPreviewCls,
  uploadImageToImgbbButtonCls,
  uploadImageToImgbbButtonHighlightedCls,
  uploadImageToImgbbButtonLoadingCls,
} from 'common/styles';
import {getPostTitleInPostCreation, getPostUrlInPostCreation} from 'common/selectors';
import Event = JQuery.Event;
import {ModuleSetupArgs} from 'common/modules';
import {RuqESModule} from 'common/RuqESModule';
import {renderMarkdown} from './renderMarkdown';

interface UploadImageToImgBbArgs {
  cfg: Config;
  file: Blob;

  onError(resp: GM.Response<any>): void;

  onSuccess(url: string): void;
}

export class CreatePostModule extends RuqESModule {
  private onLoadTitleButtonClick = (el: JQuery) => (evt: Event) => {
    debugLog('onLoadTitleButtonClick', el, evt);
    if (el.hasClass(loadTitleButtonLoadingCls)) {
      debugLog('onLoadTitleButtonClick - ignoring click, loading already in progress');
      return;
    }
    const url = String(getPostUrlInPostCreation().val());
    if (!url || (!url.startsWith('https://') && !url.startsWith('http://'))) {
      debugLog('onLoadTitleButtonClick - ignoring click, url doesn\'t look valid', url);
      return;
    }
    el.prop('disabled', true).addClass(loadTitleButtonLoadingCls);
    const enable = () => el.prop('disabled', false).removeClass(loadTitleButtonLoadingCls);
    xhr({
      method: 'GET',
      url,
      onload: resp => {
        if (resp.status !== 200) {
          return;
        }
        const parsed = $('<div>').append($(resp.response));
        const titleFromItemPropName = parsed.find('meta[itemprop=name]').prop('content');
        const titleFromTitleTag = parsed.find('title').first().text();
        debugLog('createPost - possible titles', {titleFromItemPropName, titleFromTitleTag});
        const title = titleFromItemPropName || titleFromTitleTag;
        if (!title) {
          debugLog('failed to get title', parsed);
        }
        getPostTitleInPostCreation().val(title);
        enable();
      },
      onerror: err => {
        debugLog('post loading failed', err);
        enable();
      },
    });
  };

  private appendElToUrlLabel = (el: JQuery) =>
    getPostUrlInPostCreation()
      .parent()
      .find('label')
      .append(el)
      .addClass('d-flex justify-content-between align-items-end');

  private setupLoadTitleButton = (cfg: Config['createPost']) => {
    if (!cfg?.loadTitleButton) {
      return;
    }
    if (window.location.pathname !== '/submit') {
      return;
    }

    const onlyForYouTube = false;

    const btn = $('<a>')
      .addClass('btn btn-secondary')
      .html(logoSvg)
      .append('Load title')
      .addClass(loadTitleButtonCls)
      .hide();
    if (!onlyForYouTube) {
      btn.show();
    }
    getPostUrlInPostCreation().change(evt => {
      const val = (evt.target as HTMLInputElement).value;
      const isYT = val.includes('youtube.com') || val.includes('youtu.be');
      if ((onlyForYouTube && isYT) || (!onlyForYouTube)) {
        btn.show();
      } else {
        btn.hide();
      }
    });
    this.appendElToUrlLabel(btn);
    btn.click(this.onLoadTitleButtonClick(btn));
  };

  private addLoadingEffectsToButton = (btn: JQuery) => {
    btn.prop('disabled', true).addClass(uploadImageToImgbbButtonLoadingCls);
  };

  private removeLoadingEffectsFromButton = (btn: JQuery) => {
    btn.prop('disabled', false).removeClass(uploadImageToImgbbButtonLoadingCls);
  };

  private fillUrl = (url: string) => {
    getPostUrlInPostCreation().val(url).trigger('input');
  };

  private onUploadImageToImgbbClick = (cfg: Config) => (el: JQuery) => (evt: JQuery.Event) => {
    debugLog('onUploadImageToImgbbClick', cfg, el, evt);
    if (el.hasClass(uploadImageToImgbbButtonLoadingCls)) {
      debugLog('onLoadTitleButtonClick - ignoring click, loading already in progress');
      return;
    }
    const enable = () => this.removeLoadingEffectsFromButton(el);
    const onError = (err: GM.Response<unknown>) => {
      debugLog('onError', err);
      alert(`Upload failed: ${err.status}\n${err.response}`);
      enable();
    };

    const fileInput = $('<input type="file" class="d-none" accept="image/x-png,image/gif,image/jpeg">');
    fileInput.change(evt => {
      debugLog('fileInput.change', evt);
      const files = fileInput.prop('files');
      if (!files || !files[0]) { return; }
      this.addLoadingEffectsToButton(el);
      const file = files[0];
      debugLog('got pic for upload', file);
      if (file) {
        this.uploadImageToImgBb({
          cfg,
          file,
          onSuccess: url => {
            this.fillUrl(url);
            enable();
          },
          onError,
        });
      }
    });
    $('body').append(fileInput);
    fileInput.click();
  };

  private uploadImageToImgBb = (args: UploadImageToImgBbArgs) => {
    const {cfg, file, onError, onSuccess} = args;
    debugLog('uploadImageToImgBb', {cfg, file});
    const doRequest = (image: string) => {
      debugLog('doRequest');
      const fd = new FormData();
      fd.append('image', btoa(image));
      const data = fd;
      debugLog({data});
      xhr({
        method: 'POST',
        url: `https://api.imgbb.com/1/upload?key=${cfg?.external?.imgbbKey}`,
        data: data as any, // probably incorrect typings
        onload: resp => {
          if (resp.status !== 200) { return onError(resp); }
          debugLog('imgbb upload success', resp, resp.response);
          let json = null;
          try {json = JSON.parse(resp.response);} catch (e) { }
          if (json) {
            const url = String(path(['data', 'url'])(json));
            debugLog('parsed imgbb response, url is', url, '; whole json = ', json);
            onSuccess(url);
          } else {
            alert(`Failed to parse response from ImgBB:\n${resp.response}`);
            onError(resp);
          }
        },
        onerror: err => onError(err),
      });
    };

    const reader = new FileReader();
    reader.onload = (readEvt) => {
      doRequest(readEvt.target?.result as string);
    };
    reader.readAsBinaryString(file);
  };

  private setupImgbbUploadButton = (cfg: Config) => {
    if (!cfg?.external?.imgbbKey) {
      return;
    }
    if (window.location.pathname !== '/submit') {
      return;
    }
    const btn = $('<a>')
      .addClass('btn btn-secondary')
      .html(logoSvg)
      .append('Upload image')
      .addClass(uploadImageToImgbbButtonCls);
    const removeHighlight = () => btn.removeClass(uploadImageToImgbbButtonHighlightedCls);
    const onDropOnImgBbUploadButton = (evt: Event) => {
      preventDefaults(evt);
      if (btn.hasClass(uploadImageToImgbbButtonLoadingCls)) {
        debugLog('setupImgbbUploadButton - ignoring, loading already in progress', evt);
        return;
      }
      const dt = (evt as any).dataTransfer || (evt as any).originalEvent.dataTransfer;
      const files = [...dt.files];
      debugLog('setupImgbbUploadButton', 'drop', dt || 'missing DT', files || 'missing files', evt);
      removeHighlight();
      this.addLoadingEffectsToButton(btn);
      const file = files[0];
      if (file) {
        this.uploadImageToImgBb({
          cfg,
          file,
          onError: resp => {
            this.removeLoadingEffectsFromButton(btn);
            alert(`drop upload failed: ${resp.status}\n${resp.response}`);
          },
          onSuccess: url => {
            this.removeLoadingEffectsFromButton(btn);
            this.fillUrl(url);
          },
        });
      }
    };
    btn
      .on('dragover', evt => {
        preventDefaults(evt);
        btn.addClass(uploadImageToImgbbButtonHighlightedCls)
      })
      .on('dragleave', evt => {
        preventDefaults(evt);
        removeHighlight();
      })
      .on('drop', onDropOnImgBbUploadButton)
    ;

    this.appendElToUrlLabel(btn);
    btn.click(this.onUploadImageToImgbbClick(cfg)(btn));
  };

  private setupPreview(cfg: Config) {
    if (!cfg.createPost.markdownPreview) { return; }
    const previewBox = $('<div>').addClass(postPreviewCls);
    if (cfg.post.improvedTableStyles) {
      previewBox.addClass(improvedTableCls);
    }
    const inputChangedHandler = debounce((evt: JQuery.ChangeEvent) => {
      const el = $(evt.target);
      previewBox.html(renderMarkdown(String(el.val())));
    }, 200);
    $i('post-text').on('input', inputChangedHandler);
    $('label[for=board]').before(previewBox);
    const toggleButton = genJsAnchor().addClass(postPreviewButtonCls).addClass('mx-1').html(
      $('<i>').addClass('fas fa-minus-circle').click((evt: JQuery.ClickEvent) => {
        previewBox.toggle();
        $(evt.target).toggleClass('fa-minus-circle fa-plus-circle');
      })
    );
    const label = $('<label>').prop('href', 'javascript:void 0').text('Preview').addClass('d-block mt-1')
      .append(toggleButton).append(createRuqesMark().addClass(nonAggressiveCls));
    previewBox.before(label);
  }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const creatPostCfg = cfg.createPost;
    if (!prop('silent', args || {})) {
      debugLog('setupCreatePost');
    }
    this.appendElToUrlLabel($('<i>').css({flexGrow: 10}));
    this.setupLoadTitleButton(creatPostCfg);
    this.setupImgbbUploadButton(cfg);
    this.setupPreview(cfg);
  };
}
