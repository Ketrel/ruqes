import marked, {MarkedOptions} from 'marked';
import {pipe} from 'ramda';

import {DeepPartial} from 'types/DeepPartial';

const processUserMentions = (x: string) => x.replace(
  /(?:^|\W)(@\w{3,25})/g,
  (_, x) => ` <a href="/${x}" target="_blank"><img class="profile-pic-20 mr-1" src="/${x}/pic/profile">${x}</a>`
);
const processGuildMentions = (x: string) => x.replace(
  /(?:^|\s)(\+\w{3,25})/g,
  (_, x) => ` <a href="/${x}" target="_blank"><img class="profile-pic-20 mr-1" src="/${x}/pic/profile">${x}</a>`
);

const options: DeepPartial<MarkedOptions> = {
  renderer: {
    image(href: string | null, title: string | null, text: string): string {
      const style = `max-height: 100px; max-width: 100%;`;
      return `<img class="in-comment-image rounded-sm my-2" rel="nofollow" src="${href}" style="${style}">`;
    },
    text(text: string): string {
      return pipe(
        processUserMentions,
        processGuildMentions,
      )(text);
    },
    link(href: string | null, title: string | null, text: string): string {
      return `<a href="${href}" target="_blank">${text}</a>`;
    }
  }
};
marked.use(options as MarkedOptions);

export const renderMarkdown = (x: string): string => {
  return marked(x);
};
