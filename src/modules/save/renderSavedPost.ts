import moment from 'moment';

import {PostSave} from 'common/config';
import {$c, getDomain, withTooltip} from 'common/common';
import {hideScrollbarCls, saveGuildImageCls} from 'common/styles';

const defaultGuildIcon = '/assets/images/guilds/default-guild-icon.png';
const defaultNsfwGuildIcon = '/assets/images/icons/nsfw_guild_icon.png';

export const renderSavedPost = (x: PostSave): JQuery => {
  const guildImage = x.guild ? `https://i.ruqqus.com/board/${x.guild.toLowerCase()}/profile-1.png` : '';
  const domain = '(' + getDomain(x.link || x.url) + ')';
  const dateRelative = moment(x.date).fromNow();
  const mainUrl = x.link || x.url;
  const html = `
  <div id="post-${x.id}" class="card ${x.nsfw ? 'nsfw' : ''}">
      <div class="d-flex flex-row-reverse flex-md-row flex-nowrap justify-content-end">
          <div class="card-header bg-transparent border-0 d-flex flex-row flex-nowrap pl-2 pl-md-0 p-0 mr-md-2">
              <div class="position-relative d-none d-md-block" style="z-index: 3;">
                  <a href="${mainUrl}" rel="nofollow noopener" ${x.previewModal ? 'data-toggle="modal" data-target="#expandImageModal"' : ''}>
                      <img src="${x.thumbnail}" class="post-img" alt="" ${x.previewModal ? `onclick="expandDesktopImage('${x.link}','${x.link}')"` : ''}>
                  </a>
              </div>
              <div class="d-block d-md-none" style="z-index: 3;">
                  <a href="javascript:void(0)" rel="nofollow noopener" ${x.previewModal ? 'data-toggle="modal" data-target="#expandImageModal"' : ''}>
                      <img src="${x.thumbnail}" class="post-img" alt="" ${x.previewModal ? `onclick="expandDesktopImage('${x.link}','${x.link}')"` : ''}>
                  </a>
                  <a>
                  </a>
              </div>
              <a></a>
          </div>
          <a>
          </a>
          <div class="card-block text-left x-scroll-parent my-md-auto w-100"><a>
          </a>
              <div style="height: 21px" class="d-block d-md-none mb-1"><a>
              </a>
                  <div class="post-meta text-left x-scroll ${hideScrollbarCls}" style="overflow-x: scroll;">
                        <a>
                            <span class="post-meta-guild"></span>
                        </a>
                        <a href="/+${x.guild}">+${x.guild}</a>
                        ${x.nsfw ? ` · <span class="text-danger"><i class="far fa-exclamation-triangle text-small mr-1"></i>nsfw</span>` : ''}
                        · 
                        <span data-toggle="tooltip" title="${x.dateRaw}">${dateRelative}</span> 
                        by
                        <a href="/@${x.author}" class="user-name">${x.author}</a>
                        ·
                        ${domain}
                  </div>
              </div>
              <div class="post-meta text-left d-none d-md-block mb-md-2">
                  <span class="post-meta-guild">
                      <span class="font-weight-bold">
                          <a href="/+${x.guild}" class="text-black">
                            <img src="${guildImage}" class="profile-pic-20 align-top mr-1 ${saveGuildImageCls}">
                            +${x.guild}
                          </a>
                      </span>
                      ·
                  </span>
                  <span data-toggle="tooltip" title="${x.dateRaw}">${dateRelative}</span> 
                  by <a href="/@${x.author}" class="user-name">${x.author}</a>
                  · ${domain}
                  ${x.nsfw ? ` · <span class="badge text-danger border-danger border-1 text-small-extra">nsfw</span>` : ''}
              </div>
              <h5 class="card-title post-title text-left w-lg-75 mb-0 mb-md-2">
                    <a href="${x.url}" class="stretched-link">${x.title}</a>
              </h5>
  
              <div class="row post-img-lg mb-3">
                  <div class="col px-0">
                      <a target="_blank" href="${x.url}" rel="nofollow noopener">
                        <img src="${x.thumbnail}" class="img-fluid" alt="post image">
                      </a>
                  </div>
              </div>
              <div class="post-actions mt-2 d-none d-md-block">
                  <ul class="list-inline text-right d-flex">
                      <li class="list-inline-item">
                          <a href="${x.url}">
                            <i class="fas fa-comment-dots"></i>
                            Comments
                          </a>
                        </li>
                  </ul>
              </div>
          </div>
      </div>
  
      <div class="card-footer d-block d-md-none mt-2">
          <div class="post-actions">
              <ul class="list-inline text-right d-flex">
                  <li class="list-inline-item"><a href="${x.url}"><i
                          class="fas fa-comment-dots"></i>Comments</a></li>
              </ul>
          </div>
      </div>
      <!--
${JSON.stringify(x, null, 4)}
      !-->
  </div>
  `;
  const el = $(html);
  $c(saveGuildImageCls, el).on('error', (evt: JQuery.TriggeredEvent) =>
    $(evt.target).prop('src', x.nsfw ? defaultNsfwGuildIcon : defaultGuildIcon)
  );
  return el;
};
