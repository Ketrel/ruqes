import {prop, append, filter, pipe, reverse, map, isEmpty} from 'ramda';

import {RuqESModule} from 'common/RuqESModule';
import {handleModulesAfterContentChange, ModuleSetupArgs} from 'common/modules';
import {Config, PostSave, readConfig, writeConfig} from 'common/config';
import {
  createMainMenuButtonForDesktop, createMainMenuButtonForMobile,
  debugLog,
  extractPostInfo,
  genJsAnchor,
  genNavigateToRuqESUrl,
  genRuqESUrl,
  isOnRuqESUrl,
  PostInfo,
  $i,
  createRuqesMark,
  $c
} from 'common/common';
import {getPostActionsListOnDesktop, getPostActionsListOnMobile, getPosts} from 'common/selectors';
import {nonAggressiveCls, savePostProcessedCls, saveEmptySavesButtonPlaceholderCls} from 'common/styles';
import {renderSavedPost} from './renderSavedPost';

const savePostText = 'Save';
const savedPostText = 'Saved';
const saveIconClasses = 'fas fa-save';
const savedIconClasses = 'fal fa-save';

export class SaveModule extends RuqESModule {
  private async onSaveButtonClick(el: JQuery, info: PostInfo) {
    debugLog('onSaveButtonClick', {el, info});
    const cfg = await readConfig();
    let newCfg: Config;
    const updateSavedPostsInConfig = (cfg: Config) => (over: (_: PostSave[]) => PostSave[]): Config => ({
      ...cfg,
      save: {
        ...cfg.save,
        posts: over(cfg.save.posts)
      }
    });
    if (cfg.save.posts.find(x => x.id === info.id)) {
      newCfg = updateSavedPostsInConfig(cfg)(filter((x: PostSave) => x.id !== info.id));
      SaveModule.setSaveButtonState(el, false);
    } else {
      newCfg = updateSavedPostsInConfig(cfg)(append(SaveModule.convertPostInfoToPostSave(info)));
      SaveModule.setSaveButtonState(el, true);
    }
    await writeConfig(newCfg);
  }

  private static convertPostInfoToPostSave(info: PostInfo): PostSave {
    return {
      title: info.title,
      author: info.author,
      guild: info.guild,
      id: info.id,
      nsfw: info.nsfw,
      url: info.url,
      link: info.link,
      thumbnail: info.thumbnail,
      date: info.date,
      dateRelative: info.dateRelative,
      dateRaw: info.dateRaw,
      previewModal: info.previewModal,
    };
  }

  private static setSaveButtonState(el: JQuery, alreadySaved: boolean) {
    el.text(alreadySaved ? savedPostText : savePostText);
    const icon = $('<i>').addClass('mr-2').addClass(alreadySaved ? savedIconClasses : saveIconClasses);
    el.prepend(icon);
  }

  private setupSavePostActions(cfg: Config) {
    getPosts()
      .filter((_, rawEl) => !$(rawEl).hasClass(savePostProcessedCls))
      .each((_, rawEl) => {
        const el = $(rawEl);
        el.addClass(savePostProcessedCls);
        const info = extractPostInfo(el);
        const alreadySaved = cfg.save.posts.find(x => x.id === info.id) !== undefined;
        const btn = genJsAnchor();
        btn.click((evt: JQuery.ClickEvent) => { this.onSaveButtonClick($(evt.target), info); });
        SaveModule.setSaveButtonState(btn, alreadySaved);

        getPostActionsListOnDesktop(el).append($('<li>').addClass('list-inline-item').html(btn));

        const mobilePostActions = getPostActionsListOnMobile(el);
        const shareActionOnMobile = mobilePostActions.find('i.fa-link');
        const mobileListItem = $('<li>').addClass('list-inline-item').html(btn.clone(true));
        if (shareActionOnMobile.length) {
          shareActionOnMobile.parent().parent().after(mobileListItem);
        } else {
          mobilePostActions.append(mobileListItem);
        }
      })
    ;
  }

  private setupMainMenuButton() {
    const text = 'Saved';
    const iconClasses = 'fas fa-save';
    const path = 'saved';
    const desktopButton = createMainMenuButtonForDesktop(text, genRuqESUrl(path), genNavigateToRuqESUrl(path), iconClasses);
    const mobileButton = createMainMenuButtonForMobile(text, genRuqESUrl(path), genNavigateToRuqESUrl(path), iconClasses);
    $('#navbar #navbarResponsive > ul > li:last-child a > i.fa-inbox').parent().after(desktopButton);
    $('#navbarResponsive > ul:last-child a > i.fa-envelope').parent().after(mobileButton);
  }

  private genEmptySaves() {
    const btn = genJsAnchor();
    SaveModule.setSaveButtonState(btn, false);

    const html = `
<span class="fa-stack fa-2x text-muted mb-4">
  <i class="fas fa-square text-gray-500 opacity-25 fa-stack-2x"></i>
  <i class="fas text-gray-500 fa-ghost fa-stack-1x text-lg"></i>
</span>
<h2 class="h5">Save your first piece of content!</h2>
<p class="text-muted">Don't know how?<br>Just click on "<span class="${saveEmptySavesButtonPlaceholderCls}"></span>" on a post.</p>
    `;
    const el = $('<div>').html(html).addClass('text-center mt-4');
    $c(saveEmptySavesButtonPlaceholderCls, el).html(btn);
    return el;
  }

  private async setupSavedPage(cfg: Config) {
    const page = $('<div>').addClass('mt-2');
    const caption = $('<div>')
      .addClass('font-weight-bold text-muted')
      .addClass('card d-flex justify-content-center align-items-center flex-row p-1 mt-0 mt-md-4 mb-0 mb-md-2')
      .append($('<h2>').text('Saved content').addClass('m-0'))
      .append(createRuqesMark().addClass(nonAggressiveCls).addClass('ml-2'))
    ;
    page.append(caption);
    const postsInnerContainer = $('<div>').addClass('posts');
    pipe(
      (x: PostSave[]) => reverse(x),
      map(renderSavedPost)
    )(cfg.save.posts).forEach(x => postsInnerContainer.append(x));
    if (isEmpty(cfg.save.posts)) { postsInnerContainer.append(this.genEmptySaves()); }
    const posts = $('<div>').addClass('row no-gutters mt-md-3').append($('<div>').addClass('col-12').append(postsInnerContainer));
    page.append(posts);
    $i('main-content-col').html(page);
    $('body').css({paddingTop: '42px'});
    await handleModulesAfterContentChange();
  }

  static isOnSavedPage() { return isOnRuqESUrl('saved'); }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const saveCfg = cfg.save;
    if (prop('silent', args || {})) {
      debugLog('SaveModule', saveCfg);
    }
    if (cfg.save.enabled) {
      this.setupSavePostActions(cfg);
      if (!this.firstSetupRunFinished) { this.setupMainMenuButton(); }
      if (SaveModule.isOnSavedPage() && !this.firstSetupRunFinished) {
        await this.setupSavedPage(cfg);
      }
    }
  }

  async onContentChange(args: ModuleSetupArgs, cfg: Config): Promise<unknown> {
    return this.setup(args, cfg);
  }
}
