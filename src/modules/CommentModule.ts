import {prop} from 'ramda';
import debounce from 'lodash.debounce';

import {debugLog, genTextUpDownCounters, parseUpDownCounts, printError} from 'common/common';
import {
  clsPrefix,
  commentBiggerFoldButtonsCls,
  commentPreviewCls,
  commentUpDownVotesAsTextProcessedCls,
  improvedTableCls,
  commentPreviewAppliedCls,
} from 'common/styles';
import {RuqESModule} from 'common/RuqESModule';
import {ModuleSetupArgs} from 'common/modules';
import {Config} from 'common/config';
import {getComments} from 'common/selectors';
import {renderMarkdown} from 'modules/createPost/renderMarkdown';

const genCommentId = (x: string) => `${clsPrefix}${x}`;

const processedCommentBoxMarkCls = genCommentId('processed-comment-box');

export class CommentModule extends RuqESModule {
  setupCtrlEnterToSend() {
    $('.comment-box')
      .filter((_, el) => !$(el).hasClass(processedCommentBoxMarkCls))
      .keydown(evt => {
        if (evt.key === 'Enter' && (evt.ctrlKey || evt.metaKey)) {
          debugLog('setupCtrlEnterToSend', 'CTRL+Enter detected', evt);
          const btn = $(evt.target).parent().find('a:contains(Comment)');
          if (btn.length > 1) {
            printError('setupCtrlEnterToSend', 'found multiple send buttons', btn);
          } else if (btn.length === 0) {
            printError('setupCtrlEnterToSend', 'no send button found', btn);
          } else {
            debugLog('setupCtrlEnterToSend', 'clicking on send button', btn);
            btn.click();
          }
        }
      })
      .addClass(processedCommentBoxMarkCls);
  };

  private setupBiggerFoldButtons() {
    $('body').addClass(commentBiggerFoldButtonsCls);
  }

  private setupUpDownVotesAsText() {
    getComments()
      .filter((_, rawEl) => !$(rawEl).hasClass(commentUpDownVotesAsTextProcessedCls))
      .each((_, rawEl) => {
        const el = $(rawEl);
        el.addClass(commentUpDownVotesAsTextProcessedCls);
        const pointsEl = el.find('.points');
        const [upCount, downCount] = parseUpDownCounts(pointsEl.data('originalTitle'));
        const counters = genTextUpDownCounters(upCount, downCount);
        el.find('.user-info .time-stamp').after(counters);
      })
    ;
  }

  private setupOneCommentPreview(cfg: Config, textArea: JQuery, insertPreviewBox: (_: JQuery) => void) {
    if (textArea.hasClass(commentPreviewAppliedCls)) {
      return;
    }
    textArea.addClass(commentPreviewAppliedCls);
    const previewBox = $('<div>').addClass(commentPreviewCls);
    if (cfg.post.improvedTableStyles) {
      previewBox.addClass(improvedTableCls);
    }
    const inputChangedHandler = debounce((evt: JQuery.ChangeEvent) => {
      const el = $(evt.target);
      previewBox.html(renderMarkdown(String(el.val())));
    }, 200);
    textArea.on('input', inputChangedHandler);
    insertPreviewBox(previewBox);
  }

  private setupPreview(cfg: Config) {
    const replyToPostEl = $('.comments-count ~ .comment-write');
    if (replyToPostEl.length !== 1) {
      debugLog('CommentModule', 'setupPreview', 'failed to locate reply to post element', replyToPostEl);
    } else {
      this.setupOneCommentPreview(cfg, $('textarea[name="body"]', replyToPostEl), previewBox => replyToPostEl.append(previewBox));
    }
    getComments().each((_, rawEl) => {
      const el = $(rawEl);
      const replyEl = el.next();
      if (!replyEl.prop('id').startsWith('reply-to-')) {
        printError('CommentModule', 'setupPreview', 'failed to locate reply element', {el, replyEl});
        return;
      }
      this.setupOneCommentPreview(cfg, $('textarea[name="body"]', replyEl), previewBox => replyEl.find('form').after(previewBox));
    });
  }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const commentCfg = cfg.comment;
    if (!prop('silent', args)) {
      debugLog('setupComment', commentCfg);
    }
    if (!this.firstSetupRunFinished) {
      if (commentCfg.ctrlEnterToSend) {
        setInterval(this.setupCtrlEnterToSend, 1000);
      }
      if (commentCfg.biggerFoldButtons) {
        this.setupBiggerFoldButtons();
      }
    }
    if (commentCfg.upDownVotesAsText) {
      this.setupUpDownVotesAsText();
    }
    if (commentCfg.preview) {
      this.setupPreview(cfg);
    }
  }

  async onContentChange(args: ModuleSetupArgs, cfg: Config) {
    await this.setup(args, cfg);
  }
}
