import {isEmpty, pipe, prop} from 'ramda';
import safeEval from 'notevil';

import {
  blurNsfwThumbnailCls,
  boxPostTextCls,
  semiHiddenPostCls,
  improvedTableCls,
  nonAggressiveCls,
  disablePostActionsJumpingAboveBarOnHoverCls,
  imageThumbnailTypeIconCls,
  imageThumbnailTypeIconOnMobileCls,
  closeButtonInImageDialogCls,
  imageThumbnailTypeIconFACls,
  imageThumbnailTypeIconBackgroundCls,
  postRulesAppliedCls,
  blurThumbnailCls,
  postUpDownVotesAsTextProcessedCls,
} from 'common/styles';
import {
  debugLog,
  dropWhileString,
  isArray,
  isValidCssColor,
  printError,
  isString,
  isObject,
  genTextUpDownCounters,
  parseUpDownCounts,
  extractPostInfo,
  isOnExactPathGen,
  genJsAnchor
} from 'common/common';
import {Config, ImageThumbnailIconTypeStyle, PostRule} from 'common/config';
import {ModuleSetupArgs} from 'common/modules';
import {RuqESModule} from 'common/RuqESModule';
import Match from 'ts-matcher';
import {getPosts} from 'common/selectors';
import {PostRuleAction, PostRulePostDescriptor, PostRuleAPI, LocationDescriptor} from './postRulesApiTypes';
import {SaveModule} from '../save/SaveModule';

type PostRuleActionTag = PostRuleAction['tag'];
const validPostRuleActionTags: PostRuleActionTag[] = ['semiHide', 'hide', 'setBackground', 'blurThumbnail'];

type UnsafePostRuleAction = { tag: PostRuleActionTag };
type PostRuleFunction = (post: PostRulePostDescriptor, A: PostRuleAPI, location: LocationDescriptor) => UnsafePostRuleAction | UnsafePostRuleAction[] | undefined;

const testPostRuleBody: string = `
    if (post.nsfw) return [A.setBackgroundAction('pink'), A.semiHideAction()];
    if (post.guild === 'Ruqqus') {
      return A.hideAction();
    }
    if (['WatchRedditDie'].includes(post.guild)) {
      return A.setBackgroundAction('orange');
    }
    if (post.author.toLowerCase().charAt(0) === 't') {
      return A.semiHideAction();
    }
    if (post.vote) {
      return A.setBackgroundAction('cyan');
    }
    if (location.path === '/all') {
      return A.setBackgroundAction('darkgreen');
    }
  `;
const testPostRule: PostRule = {name: 'Test post rule', body: testPostRuleBody, enabled: true};


export class PostsModule extends RuqESModule {

  private postRuleFunctions: PostRuleFunction[] = [];

  private isGuildHidden = (cfg: Config['post'], guildName = '') => {
    const hiddenGuilds = (cfg?.hidePostsFromGuilds || '').split(',').map(x => x.toLowerCase());
    const processedGuildName = dropWhileString(x => x === '+', guildName.toLowerCase());
    return hiddenGuilds.includes(processedGuildName);
  }

  private isPostFromHiddenGuild = (cfg: Config['post']) => (_: unknown, el: HTMLElement) => {
    const guildName =
      $(el).find('.post-meta-guild a').filter((_, el) => $(el).text().startsWith('+')).first().text();
    return this.isGuildHidden(cfg, guildName);
  };

  private isOnDiscoverPage = isOnExactPathGen('browse');

  private hideAlreadyJoinedGuildTiles() {
    $('#main-content-col .card-footer :not(.d-none) > .btn-secondary')
      .each((_, rawEl) => {$(rawEl).closest('.col').remove();});
  }

  private setupCloseButtonInImageDialog() {
    const expImgLinkEl = $('#desktop-expanded-image-link');
    if (expImgLinkEl.parent().find(`.${closeButtonInImageDialogCls}`).length) { return; }
    const closeLink = genJsAnchor()
      .text('Close')
      .addClass('text-gray-500 font-weight-bold text-right')
      .addClass(closeButtonInImageDialogCls)
      .click(() => $('#expandImageModal').click())
    ;
    expImgLinkEl.after(closeLink);
  }

  private setupImageThumbnailIconType(style: ImageThumbnailIconTypeStyle, background: boolean) {
    type IconGenArgs = { isImage: boolean };
    const addCommonClassesToIcon = (x: JQuery): JQuery => x
      .addClass(background ? imageThumbnailTypeIconBackgroundCls : '')
      .addClass(imageThumbnailTypeIconCls)
    ;

    const genThumbnailIconEmoji = (args: IconGenArgs): JQuery => pipe(
      (x: JQuery) => x.text(args.isImage ? '🖼' : '🔗'),
      addCommonClassesToIcon,
    )($('<div>'));
    const genThumbnailIconFA = (args: IconGenArgs) => pipe(
      (x: JQuery) => x
        .addClass(imageThumbnailTypeIconFACls)
        .html($('<i>').addClass('fas').addClass(args.isImage ? 'fa-camera' : 'fa-link')),
      addCommonClassesToIcon,
      )($('<div>'))
    ;

    $('.post-img').parent().each((_, rawEl) => {
      const el = $(rawEl);
      if (el.parent().parent().find(`.${imageThumbnailTypeIconCls}`).length) { return; }
      const isImage = el.attr('data-toggle') === 'modal';

      const icon = Match(style)
        .case('emoji', () => genThumbnailIconEmoji)
        .case('fa', () => genThumbnailIconFA)
        .exec()({isImage});

      el.parent().append(icon);
      el.closest('.card-header')
        .find('.d-md-none')
        .filter((_, rawEl) => $(rawEl).children().length > 0)
        .append(icon.clone().addClass(imageThumbnailTypeIconOnMobileCls));
    });
  }

  private static compileRule(rule: PostRule): PostRuleFunction {
    const isUnsafePostRule = (x: unknown): x is UnsafePostRuleAction =>
      isObject(x) && validPostRuleActionTags.includes((x as any).tag);
    return (post, A, location) => {
      const prg = `(function() {/* ${rule.name} */ ${rule.body}\n})()`;
      let res;
      try {
        res = safeEval(prg, {post, A, location});
      } catch (e) {
        printError(`Error while running post rule '${rule.name}':\n${e}\nRule code:\n${rule.body}`);
        return undefined;
      }
      if (!isArray(res) && !isObject(res)) {
        return;
      }
      if (isArray(res)) { return res.filter(isUnsafePostRule); }
      if (isUnsafePostRule(res)) {return res; }
      return undefined;
    };
  }

  private applyRule(el: JQuery, resultingActions: PostRuleAction[]) {
    resultingActions.forEach(action => {
      switch (action.tag) {
        case 'hide':
          el.hide();
          this.somePostsFullyHiddenCb?.();
          return;
        case 'semiHide':
          el.addClass(semiHiddenPostCls);
          return;
        case 'setBackground':
          el.css({backgroundColor: PostsModule.validateCssColor(action.color) || ''});
          return;
        case 'blurThumbnail':
          el.addClass(blurThumbnailCls);
          return;
        default:
          printError(`Unknown post action`, action, el);
          return;
      }
    })
  }

  private static validateCssColor(x: string | undefined): string | undefined {
    if (x === undefined || !isString(x)) {
      return undefined;
    }
    if (!isValidCssColor(x)) {
      printError(`Invalid CSS color: '${x}'.`, x);
      return undefined;
    }
    return x;
  }

  private applyRules() {
    const debugInfo: any[] = [];
    getPosts().filter((_, rawEl) => !$(rawEl).hasClass(postRulesAppliedCls)).each((_, rawEl) => {
      const el = $(rawEl);
      const info = extractPostInfo(el);
      const descr: PostRulePostDescriptor = { // no spread because then type-checking becomes unreliable
        title: info.title,
        author: info.author,
        guild: info.guild,
        id: info.id,
        nsfw: info.nsfw,
        score: info.score,
        vote: info.vote,
        url: info.url,
        link: info.link,
        date: info.date,
        dateRaw: info.dateRaw,
        dateRelative: info.dateRelative,
        pinned: info.pinned,
      };
      const zero: PostRuleAction[] = [];
      const resultingActions: PostRuleAction[] = this.postRuleFunctions.reduce(
        (acc: PostRuleAction[], ruleFunction: PostRuleFunction): PostRuleAction[] => {
          if (isEmpty(acc)) {
            const api: PostRuleAPI = {
              hideAction: () => ({tag: 'hide'}),
              semiHideAction: () => ({tag: 'semiHide'}),
              setBackgroundAction: (color?: string) => ({tag: 'setBackground', color}),
              blurThumbnailAction: () => ({tag: 'blurThumbnail'}),
            };
            const winLoc = window.location;
            const loc: LocationDescriptor = {
              href: winLoc.href,
              path: winLoc.pathname,
              search: winLoc.search
            };
            const ret = ruleFunction(descr, api, loc) ?? zero;
            return isArray(ret) ? ret : [ret];
          } else {
            return acc;
          }
        },
        zero
      );
      debugInfo.push({postId: descr.id, resultingAction: resultingActions, descr, info});
      el.addClass(postRulesAppliedCls);
      this.applyRule(el, resultingActions);
    });
    debugLog('applyRules', debugInfo);
  }

  private setupRulesEngine(cfg: Config) {
    const rules = [
      ...cfg.debug.enabled && cfg.debug.insertTestPostRule ? [testPostRule] : [],
      ...cfg.post.rules,
    ];

    debugLog('setupRulesEngine', cfg);
    if (!this.firstSetupRunFinished) {
      this.postRuleFunctions = rules.filter(x => x.enabled).map(PostsModule.compileRule);
    }
    this.applyRules();
  }

  private setupUpDownVotesAsText() {
    getPosts()
      .filter((_, rawEl) => !$(rawEl).hasClass(postUpDownVotesAsTextProcessedCls))
      .each((_, rawEl) => {
        const el = $(rawEl);
        el.addClass(postUpDownVotesAsTextProcessedCls);
        const [upCount, downCount] = parseUpDownCounts(el.find('.score').data('originalTitle'));
        const counters = genTextUpDownCounters(upCount, downCount);
        el.find('.post-meta').append(counters);
      })
    ;
  }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const postsCfg = cfg.post;
    if (prop('silent', args || {})) {
      debugLog('PostsModule', postsCfg);
    }
    if (postsCfg.openInNewTab) {
      $('.post-title a').prop('target', '_blank');
      $('.post-actions a > i.fa-comment-dots').parent().prop('target', '_blank');
    }
    if (postsCfg.hidePostsFromGuilds) {
      const toHide = getPosts().filter(this.isPostFromHiddenGuild(postsCfg));
      if (postsCfg.fullyHidePostsFromGuilds) {
        toHide.hide();
        this.somePostsFullyHiddenCb?.();
      } else {
        toHide.addClass(semiHiddenPostCls);
      }
    }
    if (postsCfg.blurNsfwThumbnails) {
      $('body').addClass(blurNsfwThumbnailCls);
    }
    if (postsCfg.hideAlreadyJoinedGuildsInDiscovery && this.isOnDiscoverPage()) {
      this.hideAlreadyJoinedGuildTiles();
      this.somePostsFullyHiddenCb?.();
    }
    if (postsCfg.improvedTableStyles) {
      $(`.${boxPostTextCls}, #post-body`).addClass(improvedTableCls);
    }
    if (postsCfg.lessAggressivePatronBadges) {
      $('.post-meta .badge, .user-info .badge').filter((_, el) => $(el).text().includes('Patron')).addClass(nonAggressiveCls);
    }
    if (postsCfg.disablePostActionsJumpingAboveBarOnHover) {
      $('body').addClass(disablePostActionsJumpingAboveBarOnHoverCls);
    }
    if (postsCfg.imageThumbnailIconType) {
      this.setupImageThumbnailIconType(cfg.post.imageThumbnailIconTypeStyle, cfg.post.imageThumbnailIconTypeBackground);
    }
    if (postsCfg.closeButtonInImageDialog) {
      this.setupCloseButtonInImageDialog();
    }
    if (postsCfg.rulesEngineEnabled) {
      this.setupRulesEngine(cfg);
    }
    if (postsCfg.upDownVotesAsText && !SaveModule.isOnSavedPage()) {
      this.setupUpDownVotesAsText();
    }
  }

  async onContentChange(args: ModuleSetupArgs, cfg: Config): Promise<unknown> {
    return this.setup(args, cfg);
  }
}
