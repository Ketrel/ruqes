import {clamp, merge, path, prop} from 'ramda';

import {Config} from 'common/config';
import {debugLog, genJsAnchor, getElSize, getWindowEl, isArray, isOnPostDetail, isString} from 'common/common';
import {
  boxEmbedCls,
  boxEmptyCls,
  boxImgCls,
  boxPostCommentsWithButtonCls,
  boxPostTextCls,
  expandBoxCls,
  expandBoxOpenedCls,
  expandoBtnCls,
  expandoStylesCls,
  resizableDirectImageCls,
} from 'common/styles';
import {getPostActionsList, getPosts} from 'common/selectors';
import {handleTextOfPost, isBitChuteVideo, isLbryLink, linkHandlers, LinkProcessorArgs} from 'modules/expandoButton/processors';
import {handleModulesAfterContentChange, ModuleSetupArgs} from 'common/modules';
import {RuqESModule} from 'common/RuqESModule';
import {addStyle} from 'common/domUtils';

interface MaxRecommendedImageSizeArgs {
  contentXPercent: number;
  contentYPercent: number;
}

const defaultMaxRecommendedImageSizeArgs: MaxRecommendedImageSizeArgs = {
  contentXPercent: 0.99,
  contentYPercent: 0.66,
};

export class ExpandoButtonModule extends RuqESModule {
  private createExpandButton = (cfg: Config) => genJsAnchor()
    .text(cfg.expandoButton.textClosed)
    .addClass(expandoBtnCls)
    .addClass(`${expandoBtnCls}--style-${cfg.expandoButton.style}`)
    .addClass('btn')
    .addClass('btn-secondary')
    .prop('role', 'button')
  ;

  private createBox = () => $('<div>')
    .addClass(expandBoxCls)
    .addClass(boxEmptyCls)
  ;

  static insertWrappedElementIntoBox = (box: JQuery, element: JQuery | JQuery[], wrapperClass: string = boxEmbedCls) => {
    const wrapped = $('<div>').addClass(wrapperClass);
    const toAdd = isArray(element) ? element : [element];
    toAdd.forEach(x => wrapped.append(x));
    ExpandoButtonModule.insertRawElementIntoBox(box, wrapped);
    handleModulesAfterContentChange();
  };

  static insertRawElementIntoBox = (box: JQuery, element: JQuery) => {
    box.append(element);
  };

  static setupResizing = (el: JQuery) => {
    el.addClass(resizableDirectImageCls);

    interface ResizingState {
      resizing: boolean;
      startPoint: [number, number] | null;
      startValue: number;
      value: number;
    }

    const finishSetup = () => {
      const initValue = el.width() / el.parent().width() * 100;
      el.css({maxWidth: 'none', maxHeight: 'none', width: `${initValue}%`});

      const state: ResizingState = {
        resizing: false,
        startPoint: null,
        startValue: initValue,
        value: initValue,
      };
      el.mousedown(e => {
        state.resizing = true;
        state.startPoint = [e.clientX, e.clientY];
        state.startValue = state.value;
      });
      const doc = $(document);
      doc.mouseup(e => {
        state.resizing = false;
        state.startPoint = null;
      });
      doc.mousemove(e => {
        if (!state.resizing || !state.startPoint) {
          return;
        }
        const nx = e.clientX;
        const ny = e.clientY;
        const [ox, oy] = state.startPoint;
        const dx = nx - ox;
        const dy = ny - oy;
        const dist = dx + dy;
        state.value = clamp(5, 500, state.startValue + dist / 5);
        el.css({width: `${state.value}%`});
      });
    };

    if (el.prop('naturalWidth') !== 0) {
      finishSetup();
    } else {
      el.on('load', finishSetup);
    }
  };


  static getMaxRecommendedImageSize = (parent: JQuery, args: Partial<MaxRecommendedImageSizeArgs> = {}): [number, number] => {
    const opts: MaxRecommendedImageSizeArgs = merge(defaultMaxRecommendedImageSizeArgs)(args);
    if (opts.contentXPercent < 0 || opts.contentXPercent > 1) { throw new Error(`Invalid opts.contentXPercent = ${opts.contentXPercent}`); }
    if (opts.contentYPercent < 0 || opts.contentYPercent > 1) { throw new Error(`Invalid opts.contentYPercent = ${opts.contentYPercent}`); }
    const winSize = getElSize(getWindowEl());
    const parentSize = getElSize(parent);
    return [parentSize[0] * opts.contentXPercent, winSize[1] * opts.contentYPercent];
  };

  static insertImageIntoBox = (box: JQuery, link: string, cfg: Config): JQuery => {
    const el = $('<img>').prop('src', link).prop('draggable', false).addClass(boxImgCls);
    const recSize = ExpandoButtonModule.getMaxRecommendedImageSize(box);
    el.css({
      maxWidth: recSize[0],
      maxHeight: recSize[1],
    });
    if (cfg?.expandoButton?.resize) {
      ExpandoButtonModule.setupResizing(el);
    }
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return el;
  };

  static readonly genExpandDesktopImageCallRegex = () => /expandDesktopImage\('(.*)','(.*)'\)/g;

  static extractExpandDesktopImage(el: JQuery): LinkProcessorArgs['fromExpandDesktopImageCall'] {
    const handler = el.find('.post-img').attr('onclick');
    if (!handler || !isString(handler)) { return; }
    const matches = [...handler.matchAll(this.genExpandDesktopImageCallRegex())];
    const image = path([0, 1], matches);
    const link = path([0, 2], matches);
    if (!isString(image) || !isString(link)) { return; }
    return {image, link};
  }

  private genButtonClickHandler = (el: JQuery<HTMLElement>, link: string, postUrl: string, cfg: Config) => (evt: JQuery.Event) => {
    const box = el.find(`.${expandBoxCls}`);
    const boxOpened = box.hasClass(expandBoxOpenedCls);
    const btn = $((evt as any).target);
    if (boxOpened) {
      btn.text(cfg.expandoButton.textClosed);
      box.removeClass(expandBoxOpenedCls);
      return;
    }
    btn.text(cfg.expandoButton.textOpened);
    box.addClass(expandBoxOpenedCls);
    if (box.hasClass(boxEmptyCls)) {
      box.removeClass(boxEmptyCls);
      if (!isOnPostDetail()) { handleTextOfPost(postUrl, box, cfg); }
      const fromExpandDesktopImageCall = ExpandoButtonModule.extractExpandDesktopImage(el);
      const linkProcessorArgs: LinkProcessorArgs = {box, cfg, link, postEl: el, fromExpandDesktopImageCall};
      const processed = linkHandlers.reduce((acc, x) => acc || x(linkProcessorArgs), false);
      if (!processed) {
        const unsupportedEl = $('<div>')
          .append('Unsupported link type: ')
          .append($('<code>').html($('<a>').prop('href', link).text(link)))
          .append('.');
        ExpandoButtonModule.insertWrappedElementIntoBox(box, unsupportedEl);
      }
    }
  };

  private processPostItem = (cfg: Config) => (_: unknown, domEl: HTMLElement) => {
    const el = $(domEl);
    const link = el.find('.card-header a').prop('href') || el.find('.post-title a').prop('href');
    const postUrl = el.find('.card-block .post-title a').prop('href');
    if (!link) { return; }
    el.append(this.createBox());
    const btn = this.createExpandButton(cfg);
    btn.click(this.genButtonClickHandler(el, link, postUrl, cfg));
    const actions = getPostActionsList(el);
    if (cfg.expandoButton.alignRight) {
      actions.append(btn);
    } else {
      actions.prepend(btn);
    }
    const isUnsupportedVideoEmbed = isBitChuteVideo(link) || isLbryLink(link);
    if (isUnsupportedVideoEmbed && isOnPostDetail()) {
      btn.click();
    }
  };

  private setupStyles(cfg: Config): void {
    const styles = `
      .${boxPostTextCls} { order: ${cfg.expandoButton.postTextOrder}; }
      .${boxEmbedCls} { order: ${cfg.expandoButton.embedOrder}; }
      .${boxPostCommentsWithButtonCls} { order: ${cfg.expandoButton.commentsOrder}; }
    `.trimEnd();
    addStyle(styles, expandoStylesCls);
  }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    if (!cfg?.expandoButton?.enabled) { return; }
    if (!prop('silent', args || {})) { debugLog('setupExpandoButton'); }
    getPosts().filter((_, el) => $(el).find(`.${expandoBtnCls}`).length === 0).each(this.processPostItem(cfg));
    if (!this.firstSetupRunFinished) { this.setupStyles(cfg); }
  };

  async onContentChange(args: ModuleSetupArgs, cfg: Config): Promise<unknown> {
    return this.setup(args, cfg);
  }
}
