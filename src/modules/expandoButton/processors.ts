import {drop, path} from 'ramda';

import {createTextLoader, debugLog, isArray, isString, printError, setupVoting, tryParseJSON, xhr} from 'common/common';
import {
  boxPostTextCls,
  errorTextCls,
  postLoadingCls,
  boxPostCommentsCls,
  boxPostCommentsWithButtonCls,
  boxGuildCls,
} from 'common/styles';
import {Config} from 'common/config';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

const isDirectImageLink = (x: string) => /\.(?:jpg|jpeg|png|gif|svg|webp)(?:\?.*)?$|^https:\/\/i.ruqqus.com\//i.test(x);
const handleDirectImageLink: LinkProcessor = ({link, box, cfg, fromExpandDesktopImageCall}) => {
  if (isDirectImageLink(link)) {
    ExpandoButtonModule.insertImageIntoBox(box, link, cfg);
    return true;
  }
  if (fromExpandDesktopImageCall) {
    ExpandoButtonModule.insertImageIntoBox(box, fromExpandDesktopImageCall.image, cfg);
    return true;
  }
  return false;
};

const imgurGifvLinkRegexGen = () => /https:\/\/i\.imgur\.com\/(\w+)\.(gifv|mp4)/g;
const isImgurGifvLink = (x: string) => imgurGifvLinkRegexGen().test(x);
const handleImgurGifv: LinkProcessor = ({link, box}) => {
  if (!isImgurGifvLink(link)) {
    return false;
  }
  const matches = [...link.matchAll(imgurGifvLinkRegexGen())];
  const id = path([0, 1], matches);
  const el = $(`<div class="handleImgurGifv"></div><blockquote class="imgur-embed-pub" lang="en" data-id="${id}"><a href="//imgur.com/${id}">?</a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script></div>`);
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};


const brokenImgurRegexGen = () => /^https:\/\/(?:m\.)?imgur\.com\/.*?([a-zA-Z0-9]+)$/gm;
const isBrokenImgurLink = (x: string) => brokenImgurRegexGen().test(x) && !x.includes('gallery');
const handleBrokenImgur: LinkProcessor = ({link, box, cfg}) => {
  if (!isBrokenImgurLink(link)) {
    return false;
  }
  xhr({
    method: 'GET',
    url: link,
    onload: resp => {
      const parsed = $(resp.response);
      const imgContainer = parsed.find('.post-image-container');
      const imgId = imgContainer.prop('id');
      const src = `https://i.imgur.com/${imgId}.png`;
      ExpandoButtonModule.insertImageIntoBox(box, src, cfg);
    },
  });
  return true;
};

const writeErrorToExpandButtonBox = (box: JQuery, errorText: string, retryCb: (() => void) | null = null) => {
  const errorEl = $('<div>')
    .addClass(errorTextCls)
    .append($('<span>').text(errorText))
  ;
  if (retryCb) {
    const retryButton = $('<button>')
      .text('Retry')
      .addClass('btn')
      .addClass('btn-primary')
      .addClass('ml-1')
      .click(() => {
        box.find(`.${errorTextCls}`).remove();
        retryCb();
      });
    errorEl.append(retryButton);
  }
  box.prepend(errorEl);
};
const onFetchingPostFailed = (postUrl: string, box: JQuery, cfg: Config) => (resp: GM.Response<unknown>) => {
  console.error('Failed to fetch a post.', resp);
  box.find(`.${postLoadingCls}`).remove();
  writeErrorToExpandButtonBox(
    box,
    `Failed to fetch the post. Error ${resp.status}: ${resp.statusText}`,
    () => handleTextOfPost(postUrl, box, cfg),
  );
};
export const handleTextOfPost = (postUrl: string, box: JQuery, cfg: Config) => {
  if (!isRuqqusPost(postUrl)) { printError('handleTextOfPost', 'invalid post URL', postUrl); }
  box.append(createTextLoader('Fetching post', postLoadingCls));
  xhr({
    method: 'GET',
    url: postUrl,
    onload: resp => {
      if (resp.status !== 200) {
        return onFetchingPostFailed(postUrl, box, cfg)(resp);
      }
      const parsed = $(resp.response);
      const post = parsed.find('#post-body').children().filter(':not(.embed-responsive)');
      box.find(`.${postLoadingCls}`).remove();
      const emptyPost = !post.length || post.text().trim() === '';
      const comments = cfg.expandoButton.showComments ? parsed.find('.comment-section:not(.text-center) > .comment') : $();
      const emptyComments = comments.length === 0;
      if (emptyPost && emptyComments) { return; }
      const wrappedComments = $('<div>').addClass(boxPostCommentsCls).append(comments).hide();
      setupVoting(wrappedComments);
      const showCommentsButton = $('<a>').addClass('btn btn-secondary').text('Toggle comments').click(evt => {
        const el = $(evt.target);
        el.parent().find(`.${boxPostCommentsCls}`).toggle();
      });

      ExpandoButtonModule.insertWrappedElementIntoBox(box, post, boxPostTextCls);
      if (!emptyComments) {
        ExpandoButtonModule.insertWrappedElementIntoBox(box, [
          showCommentsButton,
          wrappedComments,
        ], boxPostCommentsWithButtonCls);
      }
    },
    onerror: onFetchingPostFailed(postUrl, box, cfg),
  });
};


const isRuqqusPost = (x: string) => /^https:\/\/ruqqus\.com\/post\/.*$/.test(x);
const handleRuqqusPost: LinkProcessor = ({link}) => {
  if (!isRuqqusPost(link)) { return false; }
  // already handled by handleTextOfPost
  return true;
};


const genRuqqusGuildRegex = () => /^https:\/\/ruqqus\.com\/\+(\w+)$/g;
const isRuqqusGuild = (x: string) => genRuqqusGuildRegex().test(x);
const handleRuqqusGuild: LinkProcessor = ({link, box, cfg}) => {
  if (!isRuqqusGuild(link)) { return false; }
  const matches = [...link.matchAll(genRuqqusGuildRegex())];
  const guildName = path([0, 1], matches);
  const img = $('<img>').prop('src', `/+${guildName}/pic/profile`);
  const guildEl = $('<a>').addClass('d-flex align-items-center flex-grow-1 justify-content-center p-3')
    .prop('href', link).append(img).append($('<strong>').text(`+${guildName}`).addClass('ml-2'));
  if (cfg.post.openInNewTab) { guildEl.prop('target', '_blank'); }
  const wrapper = $('<div>').addClass(boxGuildCls).addClass('d-flex align-items-center justify-content-center')
    // .append($('<span>').addClass('mr-3').text('Guild'))
    .append(guildEl);
  ExpandoButtonModule.insertWrappedElementIntoBox(box, wrapper);
  return true;
};


const imgurAlbumPrefix = 'https://imgur.com/a/';
const isImgurAlbum = (x: string) => x.startsWith(imgurAlbumPrefix);
const handleImgurAlbum: LinkProcessor = ({link, box}) => {
  if (!isImgurAlbum(link)) {
    return false;
  }
  const albumId = drop(imgurAlbumPrefix.length, link);
  const el = $(`<div class="handleImgurAlbum embed-responsive"></div><blockquote class="imgur-embed-pub" lang="en" data-id="a/${albumId}"><a href="//imgur.com/a/${albumId}">?</a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script></div>`);
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};


const imgurGalleryPrefix = 'https://imgur.com/gallery/';
const isImgurGallery = (x: string) => x.startsWith(imgurGalleryPrefix);
const imgurApiClientId = '3d83b5e5341b638';
const handleImgurGallery: LinkProcessor = ({link, box, cfg}) => {
  if (!isImgurGallery(link)) { return false; }
  xhr({
    url: 'https://api.imgur.com/3/gallery/album/' + drop(imgurGalleryPrefix.length)(link),
    method: 'GET',
    headers: {Authorization: `Client-ID ${imgurApiClientId}`},
    onload(response: GM.Response<any>) {
      debugLog('handleImgurGallery', 'onload', response);
      const res = tryParseJSON(response.response);
      if (!res || !path(['success'])(res)) {
        writeErrorToExpandButtonBox(box, 'Imgur responded with error.');
        return;
      }
      debugLog('handleImgurGallery', 'parsed JSON, res =', res);
      const images = path(['data', 'images'])(res);
      if (!images || !isArray(images)) {
        writeErrorToExpandButtonBox(box, 'Failed to extract "images" field.');
        return;
      }
      const firstImage = images[0];
      const link = path(['link'])(firstImage);
      if (!link || !isString(link)) {
        writeErrorToExpandButtonBox(box, 'Failed to get a link from image');
        return;
      }
      ExpandoButtonModule.insertImageIntoBox(box, link, cfg)
        .prop('title', images.length > 1 ? `1 / ${images.length}` : '');
    },
    onerror(response: GM.Response<any>) {
      printError('handleImgurGallery', 'failed', response);
      writeErrorToExpandButtonBox(box, 'Imgur request failed: ' + response.status + ' - ' + String(response.response));
    },
  });
  return true;
};

const genYouTubeVideoRegex = () => /^https:\/\/(?:(?:www|m)\.)?youtube\.com\/watch\/?\?v=([\w-]+).*$|^https:\/\/youtu\.be\/([\w-]+)$/g;
const isYouTubeVideo = (x: string) => genYouTubeVideoRegex().test(x);
const handleYouTubeVideo: LinkProcessor = ({link, box}) => {
  if (!isYouTubeVideo(link)) {
    return false;
  }
  const matches = [...link.matchAll(genYouTubeVideoRegex())];
  const id = path([0, 1], matches) || path([0, 2], matches);
  if (!id) {
    console.log('failed to get YouTube video id', link);
    return false;
  }
  const el = $(`<div class="handleYouTubeVideo embed-responsive embed-responsive-16by9"><iframe width="560" height="315" src="https://www.youtube.com/embed/${id}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>`);
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};


const genBitChuteVideoRegex = () => /^https:\/\/www\.bitchute\.com\/video\/(\w+).*$/g;
export const isBitChuteVideo = (x: string) => genBitChuteVideoRegex().test(x);
const handleBitChuteVideo: LinkProcessor = ({link, box}) => {
  if (!isBitChuteVideo(link)) {
    return false;
  }
  const matches = [...link.matchAll(genBitChuteVideoRegex())];
  const id = path([0, 1], matches);
  if (!id) {
    console.log('failed to get BitChute video id', link);
    return false;
  }
  const el = $(`<div class="handleBitChuteVideo embed-responsive embed-responsive-16by9"><iframe width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/${id}/"></iframe></div>`);
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};

const genRedGifsVideoRegex = () => /^https:\/\/(?:www\.)?redgifs\.com\/watch\/([\w_\d\-]+).*$/g;
const isRedGifsVideo = (x: string) => genRedGifsVideoRegex().test(x);
const handleRedGifsVideo: LinkProcessor = ({link, box}) => {
  if (!isRedGifsVideo(link)) {
    return false;
  }
  const matches = [...link.matchAll(genRedGifsVideoRegex())];
  const id = path([0, 1], matches);
  if (!id) {
    console.log('failed to get RedGifs video id', link);
    return false;
  }
  const el = $(`<div class="handleRedGifsVideo embed-responsive embed-responsive-16by9"><iframe src='https://redgifs.com/ifr/${id}' frameborder='0' scrolling='no' allowfullscreen width='640' height='360'></iframe></div>`);
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};

const genGfycatVideoRegex = () => /^https:\/\/gfycat\.com\/([\w_\d]+).*$/g;
const isGfycatVideo = (x: string) => genGfycatVideoRegex().test(x);
const handleGfycatVideo: LinkProcessor = ({link, box}) => {
  if (!isGfycatVideo(link)) {
    return false;
  }
  const matches = [...link.matchAll(genGfycatVideoRegex())];
  const id = path([0, 1], matches);
  if (!id) {
    console.log('failed to get Gfycat video id', link);
    return false;
  }
  const el = $(`<div class="handleGfycatVideo embed-responsive embed-responsive-16by9"><iframe src='https://gfycat.com/ifr/${id}' frameborder='0' scrolling='no' allowfullscreen width='640' height='492'></iframe></div>`);
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};

const gifDeliveryNetworkRegexGen = () => /^https:\/\/(?:www\.)gifdeliverynetwork\.com\/([\w_\d]+).*$/gm;
const isGifDeliveryNetworkLink = (x: string) => gifDeliveryNetworkRegexGen().test(x);
const handleGifDeliveryNetwork: LinkProcessor = ({link, box}) => {
  if (!isGifDeliveryNetworkLink(link)) {
    return false;
  }
  const matches = [...link.matchAll(gifDeliveryNetworkRegexGen())];
  const id = path([0, 1], matches);
  if (!id) {
    console.log('failed to get GifDeliveryNetwork video id', link);
    return false;
  }
  const el = $(`<div class="handleGfycatVideo embed-responsive embed-responsive-16by9"><iframe src='https://gfycat.com/ifr/${id}' frameborder='0' scrolling='no' allowfullscreen width='640' height='492'></iframe></div>`);
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};

const imgflipLinkRegex = () => /^https:\/\/imgflip\.com\/i\/.*?([a-zA-Z0-9]+)$/gm;
const isImgflipLink = (x: string) => imgflipLinkRegex().test(x);
const handleImgflipLink: LinkProcessor = ({link, box, cfg}) => {
  if (!isImgflipLink(link)) {
    return false;
  }
  const matches = [...link.matchAll(imgflipLinkRegex())];
  const id = path([0, 1], matches);
  const src = `https://i.imgflip.com/${id}.jpg`;
  ExpandoButtonModule.insertImageIntoBox(box, src, cfg);
  return true;
};

const ibbcoLinkRegex = () => /^https:\/\/ibb.co\//gm;
const isIbbcoLink = (x: string) => ibbcoLinkRegex().test(x);
const handleIbbcoLink: LinkProcessor = ({link, box, cfg}) => {
  if (!isIbbcoLink(link)) {
    return false;
  }
  xhr({
    method: 'GET',
    url: link,
    onload: resp => {
      const parsed = $(resp.response);
      const img = parsed.find('#image-viewer-container img');
      const imgUrl = img.prop('src');
      ExpandoButtonModule.insertImageIntoBox(box, imgUrl, cfg);
    },
  });
  return true;
};


const twitterLinkRegex = () => /^https:\/\/(?:mobile\.)?twitter\.com\/([\w0-9]+)\/status\/(\d+)$/gm;
const isTwitterLink = (x: string) => twitterLinkRegex().test(x);
const handleTwitterLink: LinkProcessor = ({link, box}) => {
  if (!isTwitterLink(link)) { return false; }
  xhr({
    method: 'GET',
    url: `https://publish.twitter.com/oembed?url=${encodeURIComponent(link)}`,
    onload: resp => {
      const data = JSON.parse(resp.response);
      debugLog('handleTwitterLink', data);
      const parsed = $(data.html);
      // TODO: option to remove script
      const el = $('<div>').css({margin: '-10px 0'}).html(parsed);
      ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    },
  });
  return true;
};


const lbryLinkRegex = () => /^https:\/\/lbry.tv\/@[\w:]+\/[\w\-:]+$/;
export const isLbryLink = (x: string) => lbryLinkRegex().test(x);
const handleLbryLink: LinkProcessor = ({link, box}) => {
  if (!isLbryLink(link)) { return false; }
  xhr({
    method: 'GET',
    url: link,
    onload: resp => {
      debugLog('handleLbryLink', resp);
      const parsed = $('<div>').append($(resp.response));
      const videoUrl = parsed.find('meta[property="og:video"]').prop('content');
      const el = $(`<div class="handleLbryLink embed-responsive embed-responsive-16by9"><iframe src="${videoUrl}" allowfullscreen></iframe></div>`);
      ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    },
    onerror: (resp) => {
      printError('handleLbryLink', resp);
    },
  });
  return true;
};

export interface LinkProcessorArgs {
  link: string;
  box: JQuery;
  cfg: Config;
  fromExpandDesktopImageCall?: {
    image: string,
    link: string
  }
  postEl: JQuery;
}

export type LinkProcessor = (args: LinkProcessorArgs) => boolean;

export const linkHandlers: LinkProcessor[] = [
  handleDirectImageLink,
  handleRuqqusPost,
  handleImgurAlbum,
  handleBrokenImgur,
  handleImgurGallery,
  handleYouTubeVideo,
  handleBitChuteVideo,
  handleRedGifsVideo,
  handleGfycatVideo,
  handleGifDeliveryNetwork,
  handleImgurGifv,
  handleImgflipLink,
  handleIbbcoLink,
  handleTwitterLink,
  handleLbryLink,
  handleRuqqusGuild,
];
