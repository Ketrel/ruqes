import {prop} from 'ramda';

import {voteAnimCls, bigVoteArrowsOnMobileCls, voteAnimHandlerAppliedCls} from 'common/styles';
import {debugLog, printError} from 'common/common';
import {Config} from 'common/config';
import {ModuleSetupArgs} from 'common/modules';
import {RuqESModule} from 'common/RuqESModule';

export class VotingModule extends RuqESModule {
  private setupClickEffect() {
    $('.arrow-up, .arrow-down')
      .filter((_, rawEl) => !$(rawEl).hasClass(voteAnimHandlerAppliedCls))
      .addClass(voteAnimHandlerAppliedCls)
      .click((evt) => {
        const evtTargetEl = $(evt.target);
        let el = evtTargetEl;
        if (el.prop('tagName').toLowerCase() === 'i') {
          el = el.parent();
        }
        const isOnMobile = Boolean(el.closest('.voting.d-md-none').length);
        if (el.prop('tagName').toLowerCase() !== 'span' && el.prop('tagName').toLowerCase() !== 'div') {
          printError('clickEffect: failed to locate voting elem');
          return true;
        }
        debugLog('voting - clickEffect - click handler', 'el =', el, '; evtTargetEl = ', evtTargetEl, 'isOnMobile = ', isOnMobile, '; evt =', evt);
        const animEl = $('<i>')
          .addClass(el.prop('class').includes('up') ? 'fa-arrow-alt-up' : 'fa-arrow-alt-down')
          .addClass('fas position-absolute')
          .addClass(voteAnimCls);
        if (!isOnMobile) {
          animEl.css({left: $('#thread').length ? '18%' : '35%'});
        }
        el.prepend(animEl);
        el.css({position: 'relative'});
        setTimeout(() => animEl.remove(), 750);
        return true;
      });
  }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const votingCfg = cfg.voting;
    if (!prop('silent', args || {})) {
      debugLog('setupVoting', votingCfg);
    }
    if (votingCfg.bigVoteArrowsOnMobile) {
      $('.voting.d-md-none').addClass(bigVoteArrowsOnMobileCls);
    }
    if (votingCfg.clickEffect) {
      this.setupClickEffect();
    }
    return Promise.resolve();
  };

  async onContentChange(args: ModuleSetupArgs, cfg: Config): Promise<unknown> {
    return this.setup(args, cfg);
  }
}
