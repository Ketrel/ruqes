import {RuqESModule} from 'common/RuqESModule';
import {ModuleSetupArgs} from 'common/modules';
import {Config} from 'common/config';
import {debugLog} from 'common/common';
import {addCustomScript, addStyle} from 'common/domUtils';
import {advancedModuleCssId, advancedModuleJavaScriptId} from 'common/styles';

export class AdvancedModule extends RuqESModule {
  async setup(args: ModuleSetupArgs, cfg: Config) {
    debugLog('AdvancedModule', cfg);
    if (cfg.advanced.enabled) {
      addCustomScript(cfg.advanced.js, advancedModuleJavaScriptId);
      addStyle(cfg.advanced.css, advancedModuleCssId)
    }
  }
}
