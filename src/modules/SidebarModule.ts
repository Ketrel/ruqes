import {getRightSidebarEl} from 'common/selectors';
import {
  hideScrollbarCls,
  sidebarButtonCls,
  sidebarButtonIconCls,
  sidebarClosedCls,
  sidebarIndependentScrollCls
} from 'common/styles';
import {$c, debugLog, isOnPathOrSubpageGen, setClass} from 'common/common';
import {Config} from 'common/config';
import {RuqESModule} from 'common/RuqESModule';
import {ModuleSetupArgs} from '../common/modules';

export class SidebarModule extends RuqESModule {
  private toggleSidebar() {
    const btn = $c(sidebarButtonCls);
    btn.toggleClass('opened');
    const opened = btn.hasClass('opened');
    setClass(getRightSidebarEl(), sidebarClosedCls, !opened);
  };

  private isOnHelpPage = isOnPathOrSubpageGen('help');

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const sidebarCfg = cfg.sidebar;
    debugLog('setupSidebar', sidebarCfg);
    if (sidebarCfg.rightButton) {
      const iconEl = $('<span>').addClass(sidebarButtonIconCls);
      const btnEl = $('<a>').addClass('btn opened').addClass(sidebarButtonCls).html(iconEl).click(this.toggleSidebar);
      getRightSidebarEl().prepend(btnEl);
      if (sidebarCfg.autoHideRight) { this.toggleSidebar(); }
    }
    if (sidebarCfg.independentScroll && !this.isOnHelpPage()) {
      getRightSidebarEl().wrapInner($('<div>').addClass(sidebarIndependentScrollCls).addClass(hideScrollbarCls));
    }
  }
}
