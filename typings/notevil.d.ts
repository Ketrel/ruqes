declare module 'notevil' {
  type SafeEval = (expr: string, context: object) => unknown;
  const safeEval: SafeEval;
  export = safeEval;
}
