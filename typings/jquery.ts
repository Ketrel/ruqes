interface JQuery<TElement = HTMLElement> extends Iterable<TElement> {
  html(body: JQuery): JQuery<TElement>;
}

// doesn't work

declare namespace JQuery {
  const $: JQueryStatic | ((_: null) => JQuery);
}

declare module 'jquery' {
  const $: JQueryStatic | ((_: null) => JQuery);
  export = $;
}
