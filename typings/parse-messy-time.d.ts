declare module 'parse-messy-time' {
  type Parse = (input: string, opts?: { now: () => Date }) => Date;
  const parse: Parse;
  export = parse;
}
